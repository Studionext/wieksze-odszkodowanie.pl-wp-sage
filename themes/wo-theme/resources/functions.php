<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;



/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'wo_theme');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};


/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'wo_theme'), __('Invalid PHP version', 'wo_theme'));
}


/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'wo_theme'), __('Invalid WordPress version', 'wo_theme'));
}


/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__ . '/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'wo_theme'),
            __('Autoloader not found.', 'wo_theme')
        );
    }
    require_once $composer;
}


/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'wo_theme'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);


/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__) . '/config/assets.php',
            'theme' => require dirname(__DIR__) . '/config/theme.php',
            'view' => require dirname(__DIR__) . '/config/view.php',
        ]);
    }, true);


add_filter('wpseo_breadcrumb_links', 'success_story_unbox_yoast_seo_breadcrumb_append_link');
function success_story_unbox_yoast_seo_breadcrumb_append_link($links)
{
    global $post;
    if (is_singular('success_story')) {
        $breadcrumb = array(
            'url' => site_url('/historie-sukcesu/'),
            'text' => 'HISTORIE SUKCESU',
        );
        array_unshift($links, $breadcrumb);
    }
    return $links;
}


add_filter('wpseo_breadcrumb_links', 'blog_unbox_yoast_seo_breadcrumb_append_link');
function blog_unbox_yoast_seo_breadcrumb_append_link($links)
{
    global $post;
    if (is_singular('post') || is_tag() || is_category()) {
        $breadcrumb = array(
            'url' => site_url('/blog/'),
            'text' => 'Blog',
        );
        array_unshift($links, $breadcrumb);
    }
    return $links;
}

add_filter('wpseo_breadcrumb_links', 'hp_unbox_yoast_seo_breadcrumb_append_link');
function hp_unbox_yoast_seo_breadcrumb_append_link($links)
{
    global $post;
    if (is_singular('post') || is_page()) {
        $breadcrumb = array(
            'url' => site_url('/'),
            'text' => 'Strona główna',
        );
        array_unshift($links, $breadcrumb);
    }
    return $links;
}



function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/////////Default value for template repeter

//Lista z ptaszkiem//
add_filter('acf/load_value/key=field_5fcf963fcc464',  'acf_load_my_defaults_blist', 10, 3);

function acf_load_my_defaults_blist($value, $post_id, $field)
{

    if ($value === false) {

        $value = array();

        $value[] = array(
            'field_5fcf963fd8f86' => 'Prawo o ruchu drogowym',
        );
        $value[] = array(
            'field_5fcf963fd8f86' => 'Prawo o ruchu powietrznym',
        );
        $value[] = array(
            'field_5fcf963fd8f86' => 'Prawo o ruchu wodnym',
        );
        $value[] = array(
            'field_5fcf963fd8f86' => 'Prawo o ruchu',
        );
        $value[] = array(
            'field_5fcf963fd8f86' => 'Prawo o ruchu komunikacyjnym',
        );
        $value[] = array(
            'field_5fcf963fd8f86' => 'Prawo ubezpieczeniowe',
        );
    }

    return $value;
}

//End Lista z ptaszkiem//

//Dlaczego my//
add_filter('acf/load_value/key=field_5ff7181b02a34',  'acf_load_my_defaults_whyus', 10, 3);

function acf_load_my_defaults_whyus($value, $post_id, $field)
{

    if ($value === false) {

        $value = array();

        $value[] = array(
            'field_5ff7182c02a35' => 'Od początku do końca każdą sprawę obsługuje jeden z 5 radców prawnych, wyspecjalizowanych w wąskiej dziedzinie odszkodowań, zadośćuczynień i rent. Choć nasz zespół składa się z różnych specjalistów, jedynie Mecenasi redagują zgłoszenia szkód, pozwy i inne pisma procesowe. ',
            'field_5ff7183c02a36' => '99,8% spraw kończymy pełnym sukcesem naszych klientów',
            'field_607d2a6eb4f65' =>  'http://localhost/wieksze-odszkodowanie/wp-content/uploads/2020/12/image-8.jpg'
        );
        $value[] = array(
            'field_5ff7182c02a35' => 'Nasi prawnicy prowadzą maksymalnie po 3-4 sprawy w miesiącu. Dlatego nie skracamy postępowania kierując się szybkim i łatwym zarobkiem i nie idziemy na kompromisy. U nas liczy się jakość, a nie ilość. W przeciwieństwie do firm odszkodowawczych ograniczamy liczbę klientów, po to, by sprawę każdego z nich traktować indywidualnie.',
            'field_5ff7183c02a36' => 'Rekordowo wysokie odszkodowanie jest w zasięgu Twoich rąk, ponieważ nie pomijamy żadnych istotnych dowodów i okoliczności związanych ze sprawą',
        );
        $value[] = array(
            'field_5ff7182c02a35' => 'Otrzymasz bezpłatną obsługę wstępną oraz bezpłatne prowadzenie sprawy karnej (pozostającej w związku z Twoją sprawą cywilną). Doradzimy, czy warto występować o zwolnienie z opłat sądowych, czy lepiej i szybciej jest je uiścić. ',
            'field_5ff7183c02a36' => 'Ogranicz koszty do minimum',
        );
        $value[] = array(
            'field_5ff7182c02a35' => 'Radca prawny jest do Twojej dyspozycji na każdym etapie postępowania. Skorzystaj z jego pomocy przy przeprowadzaniu czynności procesowych, kompletowaniu dokumentów, przygotowaniu do sprawy i zawsze, gdy potrzebujesz porady lub chcesz rozwiać wątpliwości w kwestiach prawnych związanych ze śmiercią bliskich.',
            'field_5ff7183c02a36' => 'Zamień stres na pewność siebie',
        );
    }

    return $value;
}
//End Dlaczego my//

//Kroki//
add_filter('acf/load_value/key=field_5fc4cffbc1925',  'acf_load_my_defaults_steps', 10, 3);

function acf_load_my_defaults_steps($value, $post_id, $field)
{

    if ($value === false) {

        $value = array();

        $value[] = array(
            'field_5fc4d008c1926' => 'Skontaktuj się z nami.',
            'field_5fc4d0a1c1927' => '<h2>Skontaktuj się z nami</h2>
            Umów się na darmową rozmowę telefoniczną lub skorzystaj z formularza na stronie internetowej. Darmowa konsultacja obejmuje także spotkanie w kancelarii lub w razie potrzeby w Twojej siedzibie – dojeżdżamy do klientów w całej Polsce.
            
            Poznaj odpowiedzi na pytania:
            <ul>
                 <li>Czy należy Ci się odszkodowanie?</li>
                 <li>O jaką rekompensatę możesz się ubiegać?</li>
                 <li>W jaki sposób walczyć o należne świadczenie?</li>
                 <li>Jak pomogliśmy poszkodowanym w podobnych sprawach?</li>
            </ul>
            &nbsp;
            ',
        );
        $value[] = array(
            'field_5fc4d008c1926' => 'Przeanalizujemy dokumenty  i wskażemy pierwsze kroki,  w tym jakie dowody zgromadzić.',
            'field_5fc4d0a1c1927' => '<h2>Przeanalizujemy dokumenty i wskażemy pierwsze kroki, w tym jakie dowody zgromadzić.</h2>
            Ustal z doradcą korzystne warunki (więcej w punkcie 5) i podpisz umowę.',
        );
        $value[] = array(
            'field_5fc4d008c1926' => 'Kancelaria zgłosi w Twoim imieniu szkodę.',
            'field_5fc4d0a1c1927' => '<h2>Kancelaria zgłosi w Twoim imieniu szkodę.</h2>
            Jeśli chcesz szybko dostać pieniądze, skierujemy sprawę na drogę negocjacji. Znamy sprawdzone i skuteczne metody negocjacyjne i argumenty, które wytrącają karty z dłoni przeciwnika. Sprawa zakończy się szybko, bezstresowo i – jeśli wolisz – bez udziału sądu, a Ty dostaniesz swoje świadczenia.
            
            Dlaczego negocjacje z prawnikiem to dobry pomysł?
            <ul>
                 <li>Zawieramy ugody na podstawie zgromadzonego materiału i udokumentowanej krzywdy</li>
                 <li>Przygotowujemy klientów do wywiadu środowiskowego, tak by ubezpieczyciel nie mógł zaniżyć wysokości należnych świadczeń</li>
                 <li>Przeprowadzamy wszelkie możliwe dowody i wykazujemy pełen rozmiar szkody i krzywdy, by zmaksymalizować kwotę odszkodowania lub zadośćuczynienia</li>
            </ul>',
        );
        $value[] = array(
            'field_5fc4d008c1926' => 'Zyskaj silnego pełnomocnika  w sądzie.',
            'field_5fc4d0a1c1927' => '<h2>Zyskaj silnego pełnomocnika w sądzie.</h2>
            Jeśli chcesz zawalczyć o naprawdę wysoką kwotę, zatrudnij pełnomocnika i przekaż swoją sprawę do rozpatrzenia sądowi.
            
            Dlaczego warto skorzystać z pomocy pełnomocnika?
            <ul>
                 <li>Poprowadzi postępowanie od początku do końca</li>
                 <li>Skutecznie wniesie o przeprowadzenie wszystkich korzystnych dla Ciebie dowodów, np. z opinii biegłego</li>
                 <li>Zbije argumenty ubezpieczycieli wymierzone w zminimalizowanie kwoty należnych Ci świadczeń</li>
                 <li>Bezpłatnie pomoże przy innych postępowaniach związanych z Twoim wypadkiem, w tym w postępowaniu karnym.</li>
            </ul>',
        );
        $value[] = array(
            'field_5fc4d008c1926' => 'Odbierz kwotę świadczenia.',
            'field_5fc4d0a1c1927' => '<h2>Odbierz kwotę świadczenia.</h2>
            Nasze wynagrodzenie zależy od wysokości wywalczonej kwoty, które pobieramy dopiero po pozytywnym zakończeniu sprawy.
            <ul>
                 <li>Ciesz się wielokrotnie większą kwotą wygranej pomniejszoną o nasze honorarium</li>
            </ul>',
        );
    }

    return $value;
}

//End Kroki//

//Lista z ikonami//
add_filter('acf/load_value/key=field_5fc49f3c70dd2',  'acf_load_my_defaults_iconlist', 10, 3);

function acf_load_my_defaults_iconlist($value, $post_id, $field)
{

    if ($value === false) {

        $value = array();

        $value[] = array(
            'field_5fc49f4670dd4' => 'Odszkodowanie, zadośćuczynienie, renta',
        );
        $value[] = array(
            'field_5fc49f4670dd4' => 'Odszkodowanie za wypadek',
        );
        $value[] = array(
            'field_5fc49f4670dd4' => 'Odszkodowanie za śmierć bliskiej osoby',
        );
        $value[] = array(
            'field_5fc49f4670dd4' => 'Odszkodowanie za uszczerbek na zdrowiu',
        );
        $value[] = array(
            'field_5fc49f4670dd4' => 'Bezpłatna porada prawna',
        );
        $value[] = array(
            'field_5fc49f4670dd4' => 'Zgłoszenie szkody',
        );
        $value[] = array(
            'field_5fc49f4670dd4' => 'Zastępstwo procesowe i prawne',
        );
        $value[] = array(
            'field_5fc49f4670dd4' => 'Cennik',
        );
    }

    return $value;
}

//End Lista z ikonami//

//Media//
add_filter('acf/load_value/key=field_5fc54df48295d',  'acf_load_my_defaults_media', 10, 3);

function acf_load_my_defaults_media($value, $post_id, $field)
{

    if ($value === false) {

        $value = array();

        $value[] = array(
            'field_5fc54e0d856b1' => '“Sprawa dla Reportera”'
        );
        $value[] = array(
            'field_5fc54e0d856b1' => '"Vademecum odszkodowań"'
        );
        $value[] = array(
            'field_5fc54e0d856b1' => '"TV Lublin"'
        );
    }

    return $value;
}

//End Media//

//Najważniejsze punkty//
add_filter('acf/load_value/key=field_5fc5543dd5131',  'acf_load_my_defaults_important', 10, 3);

function acf_load_my_defaults_important($value, $post_id, $field)
{

    if ($value === false) {

        $value = array();

        $value[] = array(
            'field_5fc5543dd8b20' => '“99,8% wygranych spraw”',
            'field_5fc5543dd8b26' => 'Przyjmujemy nawet najbardziej wymagające sprawy o odszkodowania, zadośćuczynienia i renty po stracie bliskich lub za trwały uszczerbek na zdrowiu. Poświęcamy im całą uwagę, dlatego wygraliśmy 99,8% prowadzonych postępowań.',
        );
        $value[] = array(
            'field_5fc5543dd8b20' => 'Nie unikamy trudnych spraw',
            'field_5fc5543dd8b26' => '42% klientów spotkało się z odmową wypłaty świadczenia, zanim trafiło do naszej kancelarii. Bierzemy pod uwagę rozmiar krzywdy, okoliczności zdarzenia i skalę dowodów, łączymy to z naszymi kwalifikacjami oraz wiedzą i przekuwamy w sukces na sali sądowej. ',
        );
        $value[] = array(
            'field_5fc5543dd8b20' => 'Rekordowe odszkodowania',
            'field_5fc5543dd8b26' => 'Walczymy dla naszych klientów o rekordowo wysokie odszkodowania, które wielokrotnie sięgały kwoty <b>1&nbsp;000&nbsp;000</b>, a nawet <b>8&nbsp;300&nbsp;000 złotych</b>. Dołącz do grona naszych zadowolonych klientów.',
        );
    }

    return $value;
}

//End Najważniejsze punkty//

//Wybrane historie sukcesu//
add_filter('acf/load_value/key=field_5fca1bf70fe43',  'acf_load_my_defaults_success', 10, 3);

function acf_load_my_defaults_success($value, $post_id, $field)
{

    if ($value === false) {

        $value = array();

        $value[] = array(
            'field_5fca1bf7139d4' => '2 200 000 zł',
            'field_5fca1d3cbbdda' => 'Za śmierć dzieci i rodzeństwa w wypadku komunikacyjnym',
        );
        $value[] = array(
            'field_5fca1bf7139d4' => '1 000 000 zł',
            'field_5fca1d3cbbdda' => 'Za śmierć męża i ojca w wypadku komunikacyjnym',
        );
        $value[] = array(
            'field_5fca1bf7139d4' => '750 000 zł',
            'field_5fca1d3cbbdda' => 'Za śmierć 3 bliskich osób w wypadku',
        );
    }

    return $value;
}

//End Wybrane historie sukcesu//


// add_filter('acf/load_value/key=field_5fca1bf70fe43',  'afc_load_my_repeater_value', 10, 3);
// function afc_load_my_repeater_value($value, $post_id, $field)
// {
//     echo print_r($value);
//     return $value;
// }


function custom_admin() {
    $url = get_option('siteurl');
    $url = $url . '/wp-content/themes/wo-theme/rm.js';
    echo '<link rel="stylesheet" type="text/css" href="' . $url . '" />';
    echo '<script src="' . $url .'"></script>';
}
add_action('admin_head', 'custom_admin');