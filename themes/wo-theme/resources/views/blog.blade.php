{{--
Template name: Blog
--}}

@extends('layouts.default')

@section('main')
    <div class="l-offsetHeader"></div>
    @include('parts.common.singleHero', ['data' => $hero])
    @include('parts.common.breadcrumbs')
    <section class="t-pressInfo l-section l-borders">
          <div class="m-blogMenu-container">       
                        @php
                        $categories = get_categories( array(
                            'orderby' => 'name',
                            'order'   => 'DESC',
                            'exclude' => array(22, 43, 6, 35),
                        ) );
                        @endphp
                        <div class="m-blogMenu--mobile">
                            <button aria-expanded="false" aria-controls="header-menu" aria-label="Otwórz nawigację" class="js-burger-category a-burger">
                            <span class="a-burger__line"></span>
                            <span class="a-burger__line"></span>
                            <span class="a-burger__line"></span>
                            </button>
                            <p>Wybierz kategorię</p>
                        </div>
                        <ul class="m-blogMenu">
                        @foreach ($categories as $category)
                        @php $category_link = get_category_link($category->cat_ID);
                        echo '<li class="m-blogMenu__item"><a class="m-blogMenu__link" href="'.esc_url( $category_link ).'" title="'.esc_attr($category->name).'">'.$category->name.$category->cat_ID.'</a></li>';
                        // .$category->cat_ID.
                        @endphp
                        @endforeach
                    </div>
        <div class="l-container">
            @if (!empty($page_lead))
                <h2 class="t-pressInfo__lead">
                    {{ $page_lead }}
                </h2>
            @endif

        <div class="l-container__item">
            @if(!empty($popular_posts))
            @include('components.spacer')
            <h2 class="generic-title">POPULARNE PORADY</h2>
            <div class="t-pressInfo__articles">
                @foreach ($popular_posts as $post)
                @include('parts.single-post')
                @endforeach
                </div>   
                @endif  
        </div>
       


            <div class="l-container__item">
                @include('components.spacer')
                <h2 class="generic-title">NAJNOWSZE PORADY</h2>
                <div class="t-pressInfo__articles">
                    @php
                    $showposts =  get_field('thelatest_qty');
                    $latest_posts = get_posts([
                    'post__not_in' => [$post->ID],
                    'numberposts' =>  $showposts,
                    'orderby' => 'date'
                    ]);
                    @endphp    
                    @foreach ($latest_posts as $post)
    
                    @include('parts.single-post')
                    @endforeach
                </div>   
            </div>
          
            

            <div class="l-container__item">
                @include('components.spacer')
            <h2 class="generic-title">WSZYSTKIE PORADY</h2>
            @if (!empty($posts))
                <div class="t-pressInfo__articles">
                    @foreach ($posts as $post)
                    @include('parts.single-post')
                    @endforeach
                </div>
            @else
                <h3>Niestety, nie mamy żadnych artykułów</h3>
            @endif
            </div>
            

        </div>
    </section>

    @if (!empty($templates))
        @foreach ($templates as $template)
            @include("templates.$template->acf_fc_layout", ['data' => \App\Controllers\App::parseTemplateData($template)])
        @endforeach
    @endif

@endsection
