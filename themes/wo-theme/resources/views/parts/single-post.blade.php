@php 
$image = get_the_post_thumbnail_url($post->ID,'medium_large');
@endphp
<div class="t-pressInfo__article">
<a href="{{ get_permalink($post->ID) }}">
	<article class="m-pressTile">
		@dump($image)
		<div class="m-pressTile__image @if (!$image) no-image @endif lazyload" data-bg="{{ $image }}">
			@php
				$cat = wp_get_post_categories($post->ID);
			@endphp
			@if (!empty($cat))
				<div class="m-pressTile__tags">
					@foreach ($cat as $catId)
						<div class="m-pressTile__tag">{{get_the_category_by_ID($catId)}}</div>
					@endforeach
				</div>
			@endif
		</div>
		<div class="m-pressTile__body">
			<h3 class="m-pressTile__title">{{ $post->post_title }}</h3>
			<div class="m-pressTile__description">
			  	@php
				$q = get_post_meta($post->ID,'wordsQty');
				if($q[0]==''){
					$q[0] = '25';
				}
				else {
					$q = get_post_meta($post->ID,'wordsQty');
				}
				$str = $post->post_content;
 	        	echo wp_trim_words($str, $q[0], "");
				//  echo get_the_excerpt($post->ID)
				@endphp
			</div>
			<div class="m-pressTile__link a-button --upper">Czytaj więcej ></div>
		</div>
	</article>
</a>
</div>