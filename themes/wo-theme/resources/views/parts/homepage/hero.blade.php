<section class="t-hero --homepage l-borders">
    <div class="l-container">
        @include('components.picture',  ['acf' => 'true', 'src' => $hero->images->small->url,
             'class' => 't-hero__background',
             'modifiers' => '--consulting',
             'sources' => [
                'min-width: 600px' => $hero->images->large->url
           ]])
        <a class="t-hero__infoButton a-infoButton" href="tel:{{ \App\Controllers\App::parseNumber($globals['helpline_number']) }}">@include('svg.phone')
            <span class="text">Infolinia</span>
            <span class="number">{{ $globals['helpline_number'] }}</span>
        </a>

        <h2 class="t-hero__title">{{ auto_nbsp($data->title) }}</h2>
        <p class="t-hero__lead"> {!!   auto_nbsp($data->description) !!}</p>

        @if ($data->link)
            <a class="t-hero__button a-button" href="{{$data->link->url}}">{{ auto_nbsp($data->link->title) }}</a>
        @endif
    </div>
</section>
