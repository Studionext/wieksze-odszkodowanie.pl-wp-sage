<section class="t-textBlocks l-section l-borders">
    <div class="l-container">
        @include('components.spacer')
        <div class="l-section__header">
            <h2 class="generic-title">{{ auto_nbsp($data->title) }}</h2>
            <div class="generic-description">
                {!! auto_nbsp($data->description) !!}
            </div>
        </div>

        <div class="t-textBlock__blocks">
            @foreach ($data->blocks as $key => $block)

                <div class="t-textBlock__block block {{ $key % 2 === 0 ? 'even' : 'odd' }}">
                    <div class="block__col --col1">
                        <div class="block__image lazyload" data-bg="{{ $block->image->url }}"></div>
                    </div>
                    <div class="block__col --col2">
                        <div class="block__content cms-content">
                        {{ auto_nbsp($block->description) }}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
