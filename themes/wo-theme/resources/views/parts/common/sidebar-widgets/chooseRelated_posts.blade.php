<div class="l-sidebarWidget m-widget --related">
    
	<h2 class="title">Podobne artykuły</h2>

    @if (!empty($data->choose_posts))
        <ul class="list">
            @foreach ($data->choose_posts as $item)
			{{-- @dump($item) --}}
			@php $permalink = get_permalink( $item->ID );@endphp
			{{-- @dump($permalink) --}}
			<li>
				<a href="@php echo esc_url( $permalink ); @endphp">
				<p>{{ $item->post_title }}</p>
				<p>czytaj więcej ></p>
				</a>
			</li>
            @endforeach
        </ul>
    @endif
</div>


	
	