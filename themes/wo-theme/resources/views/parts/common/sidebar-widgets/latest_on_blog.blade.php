@php
    $posts = get_posts([
        'post__not_in' => [$post->ID],
        'numberposts' => 5,
        'orderby' => 'date'
    ]);
@endphp

@if (!empty($posts))
    <div class="l-sidebarWidget m-widget --related">
        <h2 class="title">Najnowsze artykuły</h2>
        <ul>

            @foreach ($posts as $post)
                <li>
                    <a href="{{ get_permalink($post->ID) }}">
                        <p>{{ auto_nbsp($post->post_title) }}</p>
                        <p>czytaj więcej ></p>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
@endif
