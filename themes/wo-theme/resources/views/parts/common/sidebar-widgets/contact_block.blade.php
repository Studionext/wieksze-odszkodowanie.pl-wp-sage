<article class="m-widget m-card">
    <div class="m-card__imageWrapper">
        <img src="{{ $globals['card']['image']['url'] }}" alt="">
    </div>
    <div class="m-card__body">
        <h3 class="m-card__title">{{ $globals['card']['title'] }}</h3>
        <ul class="m-card__list">
            <li class="m-card__listItem">@include('svg.address') {{ $globals['card']['address'] }}</li>
            <li class="m-card__listItem email">@include('svg.mail')
                <a href="mailto:{{ $globals['card']['email'] }}">
                    {{ $globals['card']['email'] }}
                </a>
            </li>
            <li class="m-card__listItem phone">@include('svg.phone')
                <span class="phones-wrapper">
                    <a href="tel:{{ \App\Controllers\App::parseNumber($globals['card']['phone_1']) }}">{{ $globals['card']['phone_1'] }}</a>
                    |
                    <a href="tel:{{ \App\Controllers\App::parseNumber($globals['card']['phone_2']) }}">{{ $globals['card']['phone_2'] }}</a>
                </span>
            </li>
        </ul>
        @if(is_page(200)) <!-- ID contact page -->
        <a class="m-card__button a-button --upper" href="#umow-porade-prawna">Umów się na bezpłatną poradę</a>
        @else
        <a class="m-card__button a-button --upper" href="<?= CONSULTING_URL ?>">Umów się na bezpłatną poradę</a>
        @endif
    </div>
</article>

