<div id="map" class="m-widget map" style="height: 404px; background-color: #dfdfdf;">

</div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBHSHz1Wpyx-VXxdkGpIJvi18U-u4jqzOQ&callback=initMap"
        type="text/javascript"></script>
<script>

    function initMap() {
        const center = { lat:  51.2639925, lng: 22.5640105 };
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 16,
            center: center,
            styles:
            [
  {
    "featureType": "landscape.man_made",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#f4f9fe"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  }
]
        });

        const marker = new google.maps.Marker({
            position: center,
            map: map,
            icon: 'http://localhost/wieksze-odszkodowanie/wp-content/uploads/2021/03/marker.png'
        });
        
    }
    
</script>
