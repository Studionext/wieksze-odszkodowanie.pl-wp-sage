<div class="l-sidebarWidget m-widget --category">
    <h2 class="title">Kategorie bloga</h2>

	@php
	$categories = get_categories( array(
		'orderby' => 'name',
		'order'   => 'DESC',
		'exclude' => array(22, 43, 6, 35),
	) );
	@endphp

    @if (!empty($categories))
        <ul class="list">
			@foreach ($categories as $category)
			@php $category_link = get_category_link($category->cat_ID);
			echo '<li><a href="'.esc_url( $category_link ).'" title="'.esc_attr($category->name).'">'.$category->name.'</a></li>';
			@endphp
            @endforeach
        </ul>
    @endif
</div>