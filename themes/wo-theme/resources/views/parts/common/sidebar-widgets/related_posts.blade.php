@php
    $currentPostCategories= wp_get_post_categories($post->ID);

    $related = get_posts([
        'category' => $currentPostCategories,
        'post__not_in' => [$post->ID],
        'numberposts' => 5,
        'orderby' => 'rand'
    ]);

@endphp

@if (!empty($related))
    <div class="l-sidebarWidget m-widget --related">
        <h2 class="title">Czytaj również</h2>
        <ul>

            @foreach ($related as $post)
                <li>
                    <a href="{{ get_permalink($post->ID) }}">
                        <p>{{ auto_nbsp($post->post_title) }}</p>
                        <p>czytaj więcej ></p>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
@endif
