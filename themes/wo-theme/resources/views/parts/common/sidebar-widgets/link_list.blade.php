<div class="l-sidebarWidget m-widget --links">
    <h2 class="title">{{$data->title}}</h2>

    @if (!empty($data->links))
        <ul class="list">
            @foreach ($data->links as $item)
                <li>
                    <div class="icon" style="background: url({{$item->iconlink->url}})"></div><a href="{{ $item->link->url }}" @if($item->link->target ?? '') target="{{ $item->link->target ?? ''}}" @endif >{{ $item->link->title ?? ''}}</a>
                </li>
            @endforeach
        </ul>
    @endif
</div>
