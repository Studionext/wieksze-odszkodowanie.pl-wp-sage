@if ( function_exists('yoast_breadcrumb') )
    <div class="a-breadcrumbs l-borders">
        <div class="l-container">
            {{yoast_breadcrumb( '<p id="breadcrumbs">','</p>' )}}
        </div>
    </div>
@endif
