<a class="a-infoButton --has-mb --hero-top" href="tel:{{ \App\Controllers\App::parseNumber($globals['helpline_number']) }}">@include('svg.phone')
    <span class="text">Infolinia</span>
    <span class="number">{{ $globals['helpline_number'] }}</span>
</a>
<section class="t-hero --single l-borders">
    <div class="l-container">
        @if(isset($hero))
        @include('components.picture',  ['acf' => 'true', 'src' => ($hero->images->small->url ?? ''),
             'class' => 't-hero__background',
             'modifiers' => '--consulting',
             'sources' => [
                'min-width: 600px' => ($hero->images->large->url ?? '')
           ]])



        @endif
        @php
            $title = $data->title ?? $post->post_title;
        @endphp

        <h1 class="t-hero__title">{{ auto_nbsp($title) }}</h1>

        @if (isset($data->description))
            <p class="t-hero__lead"> {!!   auto_nbsp($data->description) !!}</p>
        @endif

        @if (isset($data->link) && !empty($data->link))
            <a class="t-hero__button a-button" href="{{$data->link->url}}">{{ auto_nbsp($data->link->title) }}</a>
        @endif
    </div>
</section>
