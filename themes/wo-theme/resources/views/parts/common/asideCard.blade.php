@if (is_single())
    @if (!isset($sidebar_widgets) || empty($sidebar_widgets))

        @php
            /* SET DEFAULTS WIDGETS WHEN EMPTY */
            // $sidebar_widgets = [];
            $sidebar_widgets[] = (object) ['acf_fc_layout' => 'contact_block'];
           if (get_post_type($post->id) === 'post') {
             $sidebar_widgets[] = (object) ['acf_fc_layout' => 'category_list'];
             $sidebar_widgets[] = (object) ['acf_fc_layout' => 'related_posts'];
             $sidebar_widgets[] = (object) ['acf_fc_layout' => 'latest_on_blog'];
           }
        @endphp
    @endif
@endif

@if (isset($sidebar_widgets) && !empty($sidebar_widgets))
    @foreach ($sidebar_widgets as $widget)
        @include("parts.common.sidebar-widgets.$widget->acf_fc_layout", ['data' => $widget])
    @endforeach
@endif



