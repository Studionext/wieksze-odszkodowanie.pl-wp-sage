<section class="t-logosSlider l-borders">
    <div class="l-container">
        <div class="t-logosSlider__sliderWrapper">
            @if (!isset($noHeader))
            <h2 class="t-logosSlider__title">Widziani w</h2>
            @endif
            <div class="t-logosSlider__slider js-slickInit">
                @foreach ($data as $slide)
                <a class="custom-link" href="{{ $slide['logo-link']}}">
                    <img class="lazyload" src="{{ $slide['image']['url'] }}" alt="{{ $slide['logo-alt']}}">
                </a>
                @endforeach
            </div>
        </div>
    </div>
</section>