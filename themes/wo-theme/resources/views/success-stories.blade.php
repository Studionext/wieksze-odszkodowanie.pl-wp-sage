{{--
Template name: Success Stories
--}}

@extends('layouts.default')

@section('main')
<div class="l-offsetHeader"></div>
@include('parts.common.singleHero', ['data' => $hero])
@include('parts.common.breadcrumbs')

@if (!empty($stories))
<section class="t-successStories l-section l-borders">
    <div class="l-container">
        <div class="t-successStories__filters">
            <div class="t-successStories__filterGroup">

                <label class="label">Kategoria</label>
                <form action="@php echo site_url() @endphp/wp-admin/admin-ajax.php" method="POST" id="filter">
                    @php
                        if( $terms = get_terms( array( 'taxonomy' => 'success_story_category', 'orderby' => 'name' ) ) ) : 
                            echo '<select id="success-category-menu" class="select" name="categoryfilter"><option value="all">Wszystkie</option> ' ;
                            foreach ( $terms as $term ) :
                                echo '<option value="' . $term->term_id . '">' . $term->name . '</option>';
                            endforeach;
                            echo '</select>';
                        endif;
                    @endphp
                    @php
                    @endphp
                    <input type="hidden" name="action" value="myfilter">
                            </form>
                {{-- <label class="label">Kategoria</label>
                <select class="select">
                    <option>Wszystkie</option>
                </select> --}}
            </div>
            <div style="display:none" class="t-successStories__filterGroup">
                <label class="label">Miasto</label>
                <select class="select">
                    <option>Wszystkie</option>
                </select>
            </div>
        </div>
        <div id="response" class="js-successStories t-successStories__articles ">
           
            @foreach ($stories as $story)
            {{-- @dump($story) --}}
            <article data-category="{!! $story->cat_id!!}" data-min="{{$story->acf['min'] }}" data-max="{{$story->acf['max'] }}"
                class="t-successStories__article m-successTile js-successTile">
                <a href="{{ get_permalink($story->ID) }}">
                    @if (get_the_post_thumbnail_url($story->ID))
                    <div class="m-successTile__image lazyload" data-bg="{{ get_the_post_thumbnail_url($story->ID) }}">
                    </div>
                    @else
                    <div class="m-successTile__image empty"></div>
                    @endif
                    <div class="m-successTile__body --observe">
                        <div class="m-successTile__title">{{ $story->acf['type']['label'] }}</div>
                        <div class="m-successTile__barWrapper">
                            <div class="m-successTile__barQuotes">
                                <span>{{ $story->acf['min'] }} zł</span>
                                <span>{{ $story->acf['max'] }} zł</span>
                            </div>
                            <div class="m-successTile__bar">
                                <span data-transparent class="transparent" style="width:50%;"></span>
                                <span data-shield class="shield" style="left: 50%">
                                    <svg fill="none" viewBox="0 0 19 21">
                                        <path fill="#2B71AA"
                                            d="M19 7.773C19 13.446 15.33 18.669 9.5 21 3.826 18.731 0 13.6 0 7.773V3.58L9.5 0 19 3.58v4.193z" />
                                    </svg>
                                </span>
                                <span data-color class="color" style="width:50%;"></span>
                            </div>
                            <div class="m-successTile__barLabels">
                                <span>zaniżona kwota</span>
                                <span>wywalczona kwota</span>
                            </div>
                        </div>
                       
                        <div class="m-successTile__description">
                            {{ auto_nbsp($story->acf['description']) }}
                        </div>
                      
                        <div class="m-successTile__link a-button --upper">Czytaj więcej ></div>
                    </div>
                </a>
            </article>
            @endforeach
        </div>
    </div>
</section>
@endif

@if (!empty($templates))
@foreach ($templates as $template)
@include("templates.$template->acf_fc_layout", ['data' => \App\Controllers\App::parseTemplateData($template)])
@endforeach
@endif

@endsection