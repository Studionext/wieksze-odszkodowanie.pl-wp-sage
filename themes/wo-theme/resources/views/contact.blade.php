{{--
Template name: Contact
--}}

@extends('layouts.default')

@section('main')
    <div class="l-offsetHeader"></div>
    @include('parts.common.singleHero', ['data' => $hero])
    @include('parts.common.breadcrumbs')

    <section class="l-section t-contactBlock l-borders">
        <div class="l-container">
            <div class="t-contactBlock__col --col1">
                <h2 class="generic-title">Informacje kontaktowe</h2>
                <div class="cms-content">
                    {{ the_content() }}
                </div>
                <div class="t-contactBlock__form nf-form">
                    {!! do_shortcode('[ninja_form id=1]') !!}
                </div>
            </div>
            <div class="t-contactBlock__col --col2">
                @include('parts.common.asideCard')
            </div>
        </div>
    </section>

    @foreach ($templates as $template)
        @include("templates.$template->acf_fc_layout", ['data' => \App\Controllers\App::parseTemplateData($template)])
    @endforeach

@endsection
