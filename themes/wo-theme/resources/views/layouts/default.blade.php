<!DOCTYPE html>
<html class="no-js" lang="<?= get_bloginfo('language'); ?>">
<head>
    
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ wp_title() }}</title>
    <meta name="description" content="test">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#2B71AA">
    @if (is_page(200)) 
    @include ("critical");
    @endif
    <!-- Check if JS is enabled, remove html "no-js" class    -->
    <script>(function (html) {
            html.className = html.className.replace(/\bno-js\b/, 'js');
            if ((window.matchMedia('(prefers-reduced-motion: reduce)').matches)) {
                html.classList.add('prefers-reduced-motion')
            }
        })(document.documentElement);</script>

    @php wp_head(); @endphp

    {!! \App\inlineCss(TEMPLATE_DIR . 'inline.css') !!}

    @yield('styles.head')

</head>
<body @php body_class($bodyClass ?? '') @endphp>
    @include('layouts.includes.cookies')
<div class="l-pageWrapper">
    <header class="js-header o-headerWrapper">
        <div class="o-header__desktop">
            <ul class="o-header__highlights">
                <li>
                    <svg fill="none" viewBox="0 0 29 25">
                        <path stroke="#2B71AA" stroke-linecap="round" stroke-linejoin="round" stroke-width="4" d="M2 23l9.5-12.075 7 8.925L25.837 2m0 0l-6.395 4.941M25.837 2L27 9.412"/>
                    </svg>
                    <div>
                        <span class="hl">99,8%</span>
                        skuteczności
                    </div>
                </li>
                <li>
                    <svg fill="none" viewBox="0 0 22 21">
                        <path fill="#2B71AA" d="M14.314 7.267h.234c1.677 0 3.35-.003 5.027 0a2.6 2.6 0 011.198.268c1.246.634 1.618 2.254.767 3.395-.03.043-.062.085-.1.14.434.5.64 1.083.53 1.755-.106.67-.467 1.152-1.039 1.485.038.107.076.21.107.318.265.858-.093 1.849-.837 2.317-.086.054-.165.086-.107.23.544 1.37-.41 2.818-1.917 2.821-.785.004-1.573 0-2.358 0-1.767 0-3.533-.007-5.302.004a5.989 5.989 0 01-2.366-.472C8.038 19.48 8 19.417 8 19.292c.003-3.133.003-6.27.007-9.403 0-.075.055-.154.09-.229.02-.05.051-.093.072-.14.805-1.81 1.61-3.619 2.413-5.432a.655.655 0 00.045-.258c.003-.93.01-1.86-.004-2.79-.003-.296.114-.475.352-.615.396-.24.809-.39 1.267-.422.495-.032.898.186 1.253.494 1.14.99 1.635 2.257 1.466 3.802-.103.951-.337 1.87-.602 2.782-.014.054-.024.104-.045.186zM6.996 9.02v.203c0 3.704 0 7.404.004 11.108 0 .141-.035.229-.173.308a2.661 2.661 0 01-1.32.356c-1.042 0-2.084.015-3.125-.007C1.1 20.958.02 19.957.012 18.773a604.205 604.205 0 010-7.55c.008-1.165 1.1-2.193 2.354-2.211 1.513-.022 3.026-.007 4.54-.007.02 0 .039.007.09.014z"/>
                    </svg>
                    <div>
                        <span class="hl">2,2 mln zł</span>
                        najwyższe wywalczone świadczenie za śmierć bliskich
                    </div>
                </li>
                <li>
                    <svg fill="none" viewBox="0 0 21 21">
                        <path fill="#2B71AA" d="M10.5 0C4.71 0 0 4.71 0 10.5S4.71 21 10.5 21 21 16.29 21 10.5 16.29 0 10.5 0zM8.861 9.107l3.786 1.262a2.733 2.733 0 011.87 2.595c0 .89-.46 1.746-1.262 2.348a4.416 4.416 0 01-1.951.802v1.672H9.696v-1.672a4.415 4.415 0 01-1.951-.802c-.803-.602-1.263-1.458-1.263-2.348H8.09c0 .871 1.104 1.607 2.411 1.607s2.41-.736 2.41-1.607c0-.486-.31-.917-.771-1.07L8.353 10.63a2.733 2.733 0 01-1.87-2.595c0-.89.46-1.746 1.262-2.348a4.416 4.416 0 011.951-.802V3.214h1.608v1.672a4.416 4.416 0 011.951.802c.802.602 1.263 1.458 1.263 2.348H12.91c0-.871-1.104-1.607-2.411-1.607s-2.41.736-2.41 1.607c0 .486.31.917.771 1.07z"/>
                    </svg>
                    <div>
                        <span class="hl">Bezpłatna</span>
                        porada w sprawie odszkodowań OC
                    </div>
                </li>
            </ul>
            <div>
                <a class="a-infoButton" href="tel:{{ \App\Controllers\App::parseNumber($globals['helpline_number']) }}">@include('svg.phone')
                    <span class="text">Infolinia</span>
                    <span class="number">{{ $globals['helpline_number'] }}</span>
                </a>
            </div>
        </div>
        <div class="o-header__glass"></div>
        <div class="o-header l-header">
            <div class="l-header__col --left">
                <a href="{{ SITE_URL }}">
                    <img class="o-header__brandLogo--desktop lazyload" src="{{ \App\asset_path('img/brand-logo.png')  }}" alt="Odszkodowania zadośćuczynienia - Radca Prawny Artur Klimkiewicz">
                    <img class="o-header__brandLogo--mobile lazyload" src="{{ \App\asset_path('img/logo.svg')  }}" alt="Odszkodowania zadośćuczynienia - Radca Prawny Artur Klimkiewicz">
                </a>
            </div>
            <div class="l-header__col --right">
                <button aria-expanded="false" aria-controls="header-menu" aria-label="Otwórz nawigację" class="js-burger o-header__burger a-burger">
                    <span class="a-burger__line"></span>
                    <span class="a-burger__line"></span>
                    <span class="a-burger__line"></span>
                </button>

                <nav class="o-header__navWrapper">

                    {!! wp_nav_menu([
                            'theme_location' => 'header-menu',
                            'menu'           => 'header-menu',
                            'menu_class'     => 'o-header__navList',
                            'menu_id'        => 'header-menu',
                            'container'      => ''
                        ])   !!}

                </nav>
            </div>
        </div>
    </header>

    <div class="o-header o-header--stickyDesktop js-stickyDesktop">
        <div class="o-header__desktop">
            <div class="col --col1">
                <a href="{{ SITE_URL }}">
                    <img class="o-header__desktopImage lazyload" src="{{ \App\asset_path('img/logo-small.png') }}" alt="">
                </a>
                <ul class="o-header__highlights">
                    <li>
                        <svg fill="none" viewBox="0 0 29 25">
                            <path stroke="#2B71AA" stroke-linecap="round" stroke-linejoin="round" stroke-width="4" d="M2 23l9.5-12.075 7 8.925L25.837 2m0 0l-6.395 4.941M25.837 2L27 9.412"/>
                        </svg>
                        <div>
                            <span class="hl">99,8%</span>
                            skuteczności
                        </div>
                    </li>
                    <li>
                        <svg fill="none" viewBox="0 0 22 21">
                            <path fill="#2B71AA" d="M14.314 7.267h.234c1.677 0 3.35-.003 5.027 0a2.6 2.6 0 011.198.268c1.246.634 1.618 2.254.767 3.395-.03.043-.062.085-.1.14.434.5.64 1.083.53 1.755-.106.67-.467 1.152-1.039 1.485.038.107.076.21.107.318.265.858-.093 1.849-.837 2.317-.086.054-.165.086-.107.23.544 1.37-.41 2.818-1.917 2.821-.785.004-1.573 0-2.358 0-1.767 0-3.533-.007-5.302.004a5.989 5.989 0 01-2.366-.472C8.038 19.48 8 19.417 8 19.292c.003-3.133.003-6.27.007-9.403 0-.075.055-.154.09-.229.02-.05.051-.093.072-.14.805-1.81 1.61-3.619 2.413-5.432a.655.655 0 00.045-.258c.003-.93.01-1.86-.004-2.79-.003-.296.114-.475.352-.615.396-.24.809-.39 1.267-.422.495-.032.898.186 1.253.494 1.14.99 1.635 2.257 1.466 3.802-.103.951-.337 1.87-.602 2.782-.014.054-.024.104-.045.186zM6.996 9.02v.203c0 3.704 0 7.404.004 11.108 0 .141-.035.229-.173.308a2.661 2.661 0 01-1.32.356c-1.042 0-2.084.015-3.125-.007C1.1 20.958.02 19.957.012 18.773a604.205 604.205 0 010-7.55c.008-1.165 1.1-2.193 2.354-2.211 1.513-.022 3.026-.007 4.54-.007.02 0 .039.007.09.014z"/>
                        </svg>
                        <div>
                            <span class="hl">2,2 mln zł</span>
                            najwyższe wywalczone świadczenie za śmierć bliskich
                        </div>
                    </li>
                    <li>
                        <svg fill="none" viewBox="0 0 21 21">
                            <path fill="#2B71AA" d="M10.5 0C4.71 0 0 4.71 0 10.5S4.71 21 10.5 21 21 16.29 21 10.5 16.29 0 10.5 0zM8.861 9.107l3.786 1.262a2.733 2.733 0 011.87 2.595c0 .89-.46 1.746-1.262 2.348a4.416 4.416 0 01-1.951.802v1.672H9.696v-1.672a4.415 4.415 0 01-1.951-.802c-.803-.602-1.263-1.458-1.263-2.348H8.09c0 .871 1.104 1.607 2.411 1.607s2.41-.736 2.41-1.607c0-.486-.31-.917-.771-1.07L8.353 10.63a2.733 2.733 0 01-1.87-2.595c0-.89.46-1.746 1.262-2.348a4.416 4.416 0 011.951-.802V3.214h1.608v1.672a4.416 4.416 0 011.951.802c.802.602 1.263 1.458 1.263 2.348H12.91c0-.871-1.104-1.607-2.411-1.607s-2.41.736-2.41 1.607c0 .486.31.917.771 1.07z"/>
                        </svg>
                        <div>
                            <span class="hl">Bezpłatna</span>
                            porada w sprawie odszkodowań OC
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col --col2">
                <button aria-expanded="false" aria-controls="header-menu" aria-label="Otwórz nawigację" class="js-desktop-burger o-header__burger a-burger">
                    <span class="a-burger__line"></span>
                    <span class="a-burger__line"></span>
                    <span class="a-burger__line"></span>
                </button>
                <a class="a-infoButton" href="tel:{{ \App\Controllers\App::parseNumber($globals['helpline_number']) }}">@include('svg.phone')
                    <span class="text">Infolinia</span>
                    <span class="number">{{ $globals['helpline_number'] }}</span>
                </a>
            </div>
        </div>
        <div class="js-desktop-nav" style="display:none">
            <nav class="o-header__navWrapper">
                {!! wp_nav_menu([
                            'theme_location' => 'header-menu',
                            'menu'           => 'header-menu',
                            'menu_class'     => 'o-header__navList',
                            'menu_id'        => 'header-menu',
                            'container'      => ''
                        ])   !!}
            </nav>
        </div>
    </div>

    <main>
        @yield('main')
    </main>

    @include('layouts.includes.footer')

    @php
        wp_footer();
    @endphp
</div>
</body>
</html>
