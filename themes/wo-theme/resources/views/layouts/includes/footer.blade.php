<footer class="o-footerWrapper l-borders">
    <div class="o-footer l-container">
        <div class="o-footer__logoWrapper">
            <img data-src="{{ \App\asset_path('img\brand-logo.png') }}" src="{{ PX }}" alt="Odszkodowania zadośćuczynienia - Radca Prawny Artur Klimkiewicz" class="o-footer__logo--dektop lazyload">
            <img data-src="{{ \App\asset_path('img\logo.svg') }}" src="{{ PX }}" alt="Odszkodowania zadośćuczynienia - Radca Prawny Artur Klimkiewicz" class="o-footer__logo--mobile lazyload">
        </div>
        <div class="o-footer__col --col1">
            <ul class="o-footer__list --contact">
                <li>
                    <svg fill="none" viewBox="0 0 33 25">
                        <g fill="#fff">
                            <path d="M29.133 0H3.867A3.872 3.872 0 000 3.867v16.5a3.872 3.872 0 003.867 3.867h25.266A3.872 3.872 0 0033 20.367v-16.5A3.872 3.872 0 0029.133 0zm1.289 20.367a1.29 1.29 0 01-1.29 1.29H3.868a1.29 1.29 0 01-1.289-1.29v-16.5c0-.71.578-1.289 1.29-1.289h25.265c.71 0 1.289.578 1.289 1.29v16.5z"/>
                            <path d="M30.203 1.607L16.5 11.8 2.797 1.607 1.258 3.676 16.5 15.013 31.742 3.676l-1.539-2.069z"/>
                        </g>
                    </svg>
                    <a class="mail" href="mailto:{{ $globals['footer']['email'] }}">{{ $globals['footer']['email'] }}</a>
                </li>
                <li class="phone">
                    <svg fill="none" viewBox="0 0 19 33">
                        <path fill="#fff" d="M16.286 0H2.4A2.402 2.402 0 000 2.404v28.195A2.4 2.4 0 002.401 33h13.885a2.402 2.402 0 002.401-2.401V2.404A2.403 2.403 0 0016.286 0zM5.918 1.46h6.854c.173 0 .314.258.314.578 0 .32-.14.58-.314.58H5.918c-.175 0-.313-.26-.313-.58 0-.32.138-.578.313-.578zm3.427 29.167a1.533 1.533 0 010-3.067 1.533 1.533 0 010 3.067zm7.543-5.253H1.8V4.055h15.088v21.318z"/>
                    </svg>
                    <a href="tel:{{ \App\Controllers\App::parseNumber($globals['footer']['phone_1']) }}">{{ $globals['footer']['phone_1'] }}</a>&nbsp;/&nbsp;<a href="tel:{{ \App\Controllers\App::parseNumber($globals['footer']['phone_2']) }}">{{ $globals['footer']['phone_2'] }}</a>
                </li>
                <li>

                    <svg fill="none" viewBox="0 0 33 33">
                        <path fill="#fff" d="M13.18 29.63v3.351l-.807-.654c-.125-.101-3.097-2.52-6.11-5.987C2.108 21.557 0 17.263 0 13.578v-.365C0 5.927 5.913 0 13.18 0c7.268 0 13.181 5.927 13.181 13.213v.365c0 .31-.016.624-.046.942l-2.577-2.357C23.21 6.79 18.677 2.577 13.18 2.577c-5.85 0-10.61 4.771-10.61 10.635v.365c0 6.227 8.004 13.764 10.61 16.053zm7.909-1.85h3.858v-3.868h-3.858v3.867zM13.181 7.733c3.013 0 5.465 2.458 5.465 5.479 0 3.02-2.452 5.479-5.465 5.479-3.014 0-5.466-2.458-5.466-5.48 0-3.02 2.452-5.478 5.466-5.478zm0 2.579a2.9 2.9 0 000 5.8 2.9 2.9 0 000-5.8zm18.086 15.714l-.723-.66v4.138A3.495 3.495 0 0127.057 33h-8.142a3.495 3.495 0 01-3.487-3.495v-4.139l-.723.661-1.734-1.904 10.015-9.159L33 24.123l-1.733 1.904zm-3.295-3.013l-4.986-4.56L18 23.014v6.49c0 .506.41.918.915.918h8.142a.917.917 0 00.915-.917v-6.49z"/>
                    </svg>{{ $globals['footer']['address'] }}</li>
                <li>
                    <svg fill="none" viewBox="0 0 34 30">
                        <g fill="#fff">
                            <path d="M30.165 1.938H11.588A2.911 2.911 0 008.85 0H6.782c-1.263 0-2.34.81-2.74 1.938H2.907A2.91 2.91 0 000 4.844v19.378a2.91 2.91 0 002.907 2.907h1.135a2.911 2.911 0 002.74 1.937H8.85c1.263 0 2.34-.81 2.74-1.937h18.576a2.91 2.91 0 002.906-2.907V4.844a2.91 2.91 0 00-2.906-2.906zM3.875 25.19h-.968a.97.97 0 01-.97-.969V4.844a.97.97 0 01.97-.968h.969V25.19zm5.943.969a.97.97 0 01-.969.969H6.782a.97.97 0 01-.969-.97V2.908a.97.97 0 01.97-.97h2.066a.97.97 0 01.969.97V26.16zm21.315-1.938a.97.97 0 01-.968.969h-18.41V3.876h18.41a.97.97 0 01.968.968v19.378z"/>
                            <path d="M16.6 21.315h-1.938a.969.969 0 000 1.938H16.6a.969.969 0 000-1.938zM22.413 21.315h-1.937a.969.969 0 000 1.938h1.938a.969.969 0 000-1.938zM28.227 21.315h-1.938a.969.969 0 000 1.938h1.938a.969.969 0 000-1.938zM16.6 17.44h-1.938a.969.969 0 000 1.937H16.6a.969.969 0 000-1.938zM22.413 17.44h-1.937a.969.969 0 100 1.937h1.938a.969.969 0 000-1.938zM28.227 17.44h-1.938a.969.969 0 100 1.937h1.938a.969.969 0 000-1.938zM16.6 13.565h-1.938a.969.969 0 000 1.937H16.6a.969.969 0 000-1.938zM22.413 13.565h-1.937a.969.969 0 100 1.937h1.938a.969.969 0 000-1.938zM28.227 13.565h-1.938a.969.969 0 100 1.937h1.938a.969.969 0 000-1.938zM28.227 5.813H14.662a.969.969 0 00-.969.97v3.875c0 .535.434.969.97.969h13.564a.969.969 0 00.968-.97V6.783a.969.969 0 00-.968-.969zm-.97 3.876H15.632V7.751h11.627V9.69z"/>
                        </g>
                    </svg>
                    <a href="tel:{{ \App\Controllers\App::parseNumber($globals['footer']['phone_landline']) }}">{{ $globals['footer']['phone_landline'] }}</a>
                </li>
            </ul>

            <a href="<?= CONSULTING_URL ?>" class="o-footer__button a-button">Skorzystaj z darmowej porady prawnej</a>

        </div>
        <div class="o-footer__col --col2">
            {!! wp_nav_menu([
                'theme_location' => 'footer-menu_one',
                'menu'           => 'footer-menu_one',
                'menu_class'     => 'o-footer__list --nav --nav1',
                'menu_id'        => 'footer-menu_one',
                'container'      => ''
            ]) !!}
        </div>
        <div class="o-footer__col --col3">
            {!! wp_nav_menu([
            'theme_location' => 'footer-menu_two',
            'menu'           => 'footer-menu_two',
            'menu_class'     => 'o-footer__list --nav --nav2',
            'menu_id'        => 'footer-menu_two',
            'container'      => ''
        ]) !!}
        </div>
        <div class="o-footer__col --col4">
            <div class="o-footer__cms cms-content">
                {!! $globals['footer']['text_box'] !!}
            </div>
        </div>
    </div>
</footer>
<div class="o-footer__copy">
    &copy; {{ date('Y') }} Made with passion by&nbsp;<a rel="nofollow" href="https://awkn.pro">Awaken</a>
</div>
