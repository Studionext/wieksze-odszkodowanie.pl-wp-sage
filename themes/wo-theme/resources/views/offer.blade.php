{{--
Template name: Offer
--}}

@extends('layouts.default')

@section('main')
    <div class="l-offsetHeader"></div>
    @include('parts.common.singleHero', ['data' => $hero])
    @include('parts.common.breadcrumbs')
    <section class="l-section t-withSidebar l-borders">
        <div class="l-container">
            <div class="t-withSidebar__col --col1">
                <div class="cms-content">
                    {{ the_content() }}
                </div>
            </div>
            <div class="t-withSidebar__col --col2">
                @include('parts.common.asideCard')
            </div>
        </div>
    </section>

    @if (!empty($after_post))
        @foreach ($after_post as $template)
            @include("templates.after_post.$template->acf_fc_layout", ['data' => \App\Controllers\App::parseTemplateData($template)])
        @endforeach
    @endif

    @if (!empty($templates))
        @foreach ($templates as $template)
            @include("templates.$template->acf_fc_layout", ['data' => \App\Controllers\App::parseTemplateData($template)])
        @endforeach
    @endif

@endsection
