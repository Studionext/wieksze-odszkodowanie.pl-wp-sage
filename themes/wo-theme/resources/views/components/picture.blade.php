@php
    $class = $class ?? null;
    $imgClass = $imgClass ?? null;
    $alt = $alt ?? null;
    $acf = $acf ?? false;
@endphp

<picture @if($class) class="{{ $class }} a-picture lazyload" @endif>
    @if (!empty($sources))
        @foreach ($sources as $media => $image)
            @if ($acf)
                <source srcset="{{ PX }}" media="({{ $media }})" data-srcset="{{ $image }}">
            @else
                <source srcset="{{ PX }}" media="({{ $media }})" data-srcset="{{ \App\asset_path("img/$image") }}">
            @endif
        @endforeach
    @endif

    @if ($acf)
        <img src="{{ PX }}" data-src="{{ $src }}" alt="{{ $alt }}"
             class="lazyload @if($imgClass) {{ $imgClass }} @endif">
    @else
        <img src="{{ PX }}" data-src="{{ \App\asset_path("img/$src") }}" alt="{{ $alt }}"
             class="lazyload @if($imgClass) {{ $imgClass }} @endif">
    @endif

</picture>
