{{--
Template name: 404
--}}
<div class="error404">
    @extends('layouts.default')

    @section('main')
    <div class="l-offsetHeader"></div>
    <section class="t-hero --single l-borders">
        <div class="l-container">
            <picture class="t-hero__background a-picture lazyloaded">
                <source srcset="http://localhost/wieksze-odszkodowanie/wp-content/uploads/2021/05/scott-graham-OQMZwNd3ThU-unsplash-1.jpg"
                    media="(min-width: 600px)"
                    data-srcset="http://localhost/wieksze-odszkodowanie/wp-content/uploads/2021/05/scott-graham-OQMZwNd3ThU-unsplash-1.jpg">
                <img src="http://localhost/wieksze-odszkodowanie/wp-content/uploads/2021/05/scott-graham-OQMZwNd3ThU-unsplash-1.jpg"
                    data-src="http://localhost/wieksze-odszkodowanie/wp-content/uploads/2021/05/scott-graham-OQMZwNd3ThU-unsplash-1.jpg" alt=""
                    class=" lazyloaded">
            </picture>
            <h1 class="t-hero__title">Błąd 404</h1>
            <p class="t-hero__lead">Niestety, strona której szukasz nie istnieje.</p>
        </div>
    </section>

    <section class="l-section t-withSidebar l-borders">
        <div class="l-container">
            <div class="t-withSidebar__col --col1">
                <div class="cms-content">
					@include('parts.common.breadcrumbs')
                    <h2 class="">Szukasz informacji?</h2>
                    <p>Szukasz informacji o odszkodowaniach? Zadzwoń, odpowiemy na Twoje pytania na temat odszkodowania z OC za darmo.</p>
                    <a class="a-infoButton" href="tel:+48518445012"><svg class="svg-phone" fill="none" viewBox="0 0 20 20">
                            <path fill="currentColor"
                                d="M18.395 13.128c-1.224 0-2.427-.192-3.566-.568-.558-.19-1.245-.016-1.585.334l-2.249 1.697C8.387 13.2 6.781 11.593 5.407 9.005l1.648-2.19a1.617 1.617 0 00.398-1.638 11.373 11.373 0 01-.57-3.572C6.883.72 6.163 0 5.278 0H1.605C.72 0 0 .72 0 1.605 0 11.748 8.252 20 18.395 20 19.28 20 20 19.28 20 18.395v-3.662c0-.885-.72-1.605-1.605-1.605z">
                            </path>
                        </svg>
                        <span class="text">Infolinia</span>
                        <span class="number">+48 518 445 012</span>
                    </a>

                </div>

            </div>


            <div class="t-withSidebar__col --col2">
                @include('parts.common.sidebar-widgets.contact_block')
            </div>
        </div>
    </section>

    <section class="t-3posts l-section l-borders">
        <div class="t-3posts__bg"></div>
        <div class="l-container">
            <div class="a-spacer"><span></span></div>
            <div class="l-section__header">
                <h2 class="generic-title">Zobacz jak pomogliśmy naszym klientom otrzymać rekordowe odszkodowania</h2>
                <div class="generic-description">
                    <p>Firmy ubezpieczeniowe robią wszystko, by wypłacić poszkodowanym jak najniższe odszkodowanie. Nie musisz się na&nbsp;to godzić
                        i&nbsp;z odpowiednią pomocą możesz wywalczyć sprawiedliwe odszkodowanie za&nbsp;doznaną szkodę.</p>
                </div>
            </div>
            <div class="t-3posts__posts">
                <article class="t-3posts__post post"> <a
                        href="https://wieksze-odszkodowanie.pl/historie-sukcesu/smierc-dzieci-i-rodzenstwa-w-wypadku-komunikacyjnym/">
                        <p class="post__title"> <span class="amount">2 200 000 zł</span> <br> <span class="text">Za śmierć dzieci i&nbsp;rodzeństwa
                                w&nbsp;wypadku komunikacyjnym</span> </p>
                        <div class="post__button a-button --white"> Czytaj więcej </div>
                    </a> </article>
                <article class="t-3posts__post post"> <a
                        href="https://wieksze-odszkodowanie.pl/historie-sukcesu/smierc-meza-i-ojca-w-wypadku-komunikacyjnym/">
                        <p class="post__title"> <span class="amount">1 000 000 zł</span> <br> <span class="text">Za śmierć męża i&nbsp;ojca
                                w&nbsp;wypadku komunikacyjnym</span> </p>
                        <div class="post__button a-button --white"> Czytaj więcej </div>
                    </a> </article>
                <article class="t-3posts__post post"> <a
                        href="https://wieksze-odszkodowanie.pl/historie-sukcesu/smierc-3-osob-wskutek-wypadku-komunikacyjnego-i-doznanych-obrazen-ciala/">
                        <p class="post__title"> <span class="amount">750 000 zł</span> <br> <span class="text">Za śmierć 3 bliskich osób
                                w&nbsp;wypadku</span> </p>
                        <div class="post__button a-button --white"> Czytaj więcej </div>
                    </a> </article>
            </div>
        </div>
    </section>


    <section class="t-bookVisit  l-section no-spacer l-borders has-bg-lightblue">
        <div class="l-section__header">
            <h2 class="generic-title">Umów się na&nbsp;bezpłatną poradę prawną</h2>
            <div class="generic-description">
                <p>Zrób pierwszy krok do&nbsp;odzyskania Twoich pieniędzy. Wypełnij formularz, a&nbsp;my skontaktujemy się z&nbsp;Tobą
                    i&nbsp;bezpłatnie ustalimy, jak możemy Ci pomóc. Gwarantujemy dyskrecję, wysoką skuteczność i&nbsp;rekordowe odszkodowanie.</p>

            </div>
        </div>
        <div class="nf-form nf-form--visit"><?= do_shortcode( '[ninja_form id=2]' )?></div>
    </section>

    <section class="t-richList --highlights l-section l-borders has-bg-blue">
        <div class="l-container">

            <ul class="t-richList__list">
                <li class="t-richList__listItem">
                    <div class="header">
                        <img data-src="http://localhost/wieksze-odszkodowanie/wp-content/uploads/2020/11/Group-1-1.png" alt=""
                            class="image lazyloaded" src="http://localhost/wieksze-odszkodowanie/wp-content/uploads/2020/11/Group-1-1.png">
                        <div class="title">99,8% wygranych spraw</div>
                    </div>
                    <div class="description">Przyjmujemy nawet najbardziej wymagające sprawy o&nbsp;odszkodowania, zadośćuczynienia i&nbsp;renty
                        po&nbsp;stracie bliskich lub&nbsp;za&nbsp;trwały uszczerbek na&nbsp;zdrowiu. Poświęcamy im całą uwagę, dlatego&nbsp;wygraliśmy
                        99,8% prowadzonych postępowań.</div>
                </li>
                <li class="t-richList__listItem">
                    <div class="header">
                        <img data-src="http://localhost/wieksze-odszkodowanie/wp-content/uploads/2020/11/Group-2.png" alt="" class="image lazyloaded"
                            src="http://localhost/wieksze-odszkodowanie/wp-content/uploads/2020/11/Group-2.png">
                        <div class="title">bezpłatne rekordowe odszkodowania</div>
                    </div>
                    <div class="description">Oferujemy bezpłatną obsługę wstępną, darmowe porady w&nbsp;sprawach o&nbsp;odszkodowania oraz&nbsp;dojazd
                        do&nbsp;klienta na&nbsp;koszt Kancelarii. Zminimalizuj koszty i&nbsp;pomnóż satysfakcję z&nbsp;wywalczonych pieniędzy</div>
                </li>
                <li class="t-richList__listItem">
                    <div class="header">
                        <img data-src="http://localhost/wieksze-odszkodowanie/wp-content/uploads/2020/11/Group-268.png" alt=""
                            class="image lazyloaded" src="http://localhost/wieksze-odszkodowanie/wp-content/uploads/2020/11/Group-268.png">
                        <div class="title">8,3 największe wywalczone odszkodowanie</div>
                    </div>
                    <div class="description">Przyjmujemy nawet najbardziej wymagające sprawy o&nbsp;odszkodowania, zadośćuczynienia i&nbsp;renty
                        po&nbsp;stracie bliskich lub&nbsp;za&nbsp;trwały uszczerbek na&nbsp;zdrowiu. Poświęcamy im całą uwagę, dlatego&nbsp;wygraliśmy
                        99,8% prowadzonych postępowań.</div>
                </li>
            </ul>
            <div class="t-richList__buttonWrapper">
                <a href="http://localhost/wieksze-odszkodowanie/historie-sukcesu/" class="a-button --gray">Zobacz inne historie</a>
            </div>
        </div>
    </section>





    @endsection
</div>
