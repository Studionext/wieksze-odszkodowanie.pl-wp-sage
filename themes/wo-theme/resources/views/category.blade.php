{{--
Template name: Category
--}}

@extends('layouts.default')

@section('main')

    <div class="l-offsetHeader"></div>
    {{-- @include('parts.common.singleHero', ['data' => $hero]) --}}

    <a class="a-infoButton --has-mb --hero-top" href="tel:{{ \App\Controllers\App::parseNumber($globals['helpline_number']) }}">@include('svg.phone')
    <span class="text">Infolinia</span>
    <span class="number">{{ $globals['helpline_number'] }}</span>
</a>
<section class="t-hero --single l-borders">
    <div class="l-container">
            <picture class="t-hero__background a-picture lazyloaded">
                <source srcset="http://localhost/wieksze-odszkodowanie/wp-content/uploads/2020/12/corinne-kutz-tMI2_-r5Nfo-unsplash-1.jpg" media="(min-width: 600px)" data-srcset="http://localhost/wieksze-odszkodowanie/wp-content/uploads/2020/12/corinne-kutz-tMI2_-r5Nfo-unsplash-1.jpg">
                <img src="http://localhost/wieksze-odszkodowanie/wp-content/uploads/2020/12/scott-graham-OQMZwNd3ThU-unsplash-1.jpg" data-src="http://localhost/wieksze-odszkodowanie/wp-content/uploads/2020/12/scott-graham-OQMZwNd3ThU-unsplash-1.jpg" alt="" class=" ls-is-cached lazyloaded">
            </picture>
        <h1 class="t-hero__title">  @php single_term_title()@endphp</h1>
    </div>
</section>
    @include('parts.common.breadcrumbs')
    <section class="t-pressInfo l-section l-borders">
        <div class="l-container">
            @php $description = category_description(); @endphp
            @if (!empty( $description))
                <h2 class="t-pressInfo__lead">
                    @php
                    echo  $description
                    @endphp
                </h2>
             @endif   
<div class="t-pressInfo__articles">
    @php global $post @endphp
            @if (have_posts())
            @while(have_posts())
                {{ the_post() }}
                        @php
                            $image = get_the_post_thumbnail_url($post->ID);
                            // var_dump($post);
                        @endphp
                        <div class="t-pressInfo__article">
                            <a href="{{ get_permalink($post->ID) }}">
                                <article class="m-pressTile">
                                    <div class="m-pressTile__image @if (!$image) no-image @endif lazyload" data-bg="{{ $image }}">
                                        @php
                                            $cat = wp_get_post_categories($post->ID);
                                        @endphp

                                        @if (!empty($cat))
                                            <div class="m-pressTile__tags">
                                                @foreach ($cat as $catId)
                                                    <div class="m-pressTile__tag">{{get_the_category_by_ID($catId)}}</div>
                                                @endforeach

                                            </div>
                                        @endif
                                    </div>
                                    <div class="m-pressTile__body">
                                        <h3 class="m-pressTile__title">{{ $post->post_title }}</h3>
                                        <div class="m-pressTile__description">
                                            {!! get_the_excerpt($post->ID) !!}
                                        </div>
                                        <div class="m-pressTile__link a-button --upper">Czytaj więcej ></div>
                                    </div>
                                </article>
                            </a>
                        </div>        
            @endwhile
            @endif
            @php wp_reset_postdata(); @endphp
</div>
{{-- <div class="m-pressTile__link a-button --upper">Wróć na blog &#8617;</div> --}}
    </section>

    @if (!empty($templates))
        @foreach ($templates as $template)
            @include("templates.$template->acf_fc_layout", ['data' => \App\Controllers\App::parseTemplateData($template)])
        @endforeach
    @endif

@endsection


