@php

    $posts = $data['list'];

    if (empty($posts)) {
        $posts = get_posts([
        'category' => [6],
        'numberposts' => 5,
    ]);
    }
@endphp


<section class="t-blockSlider l-section no-spacer l-borders has-bg-blue">
    <div class="t-blockSlider__bg"></div>
    <div class="l-container">
        <div class="l-section__header">
            <h2 class="generic-title">{{ auto_nbsp($data['title']) }}</h2>
            <div class="generic-description">
                {!! auto_nbsp($data['description']) !!}
            </div>
        </div>
    </div>


    <div class="l-container --slider">
        <div class="js-slickInit" data-slick="blocksSlider">
            @foreach($posts as $post)
                @php
                    // acf provides posts as arrays, need to check for it
                    // and convert to obj to be equal with the results from get_posts;
                    if (!is_object($post) && isset($post['post'])) {
                        $post = (object) $post['post'];
                    }
                    $bgImg = get_the_post_thumbnail_url($post->ID);
                @endphp
               
                <article class="t-blockSlider__post post">
                    <a href="{{ get_permalink($post->ID) }}" class="post__link outline-none">
                    <div class="outline-none">
                        <div class="post__image lazyload {{ $bgImg ? null : 'no-bg' }}" data-bg="{{ $bgImg }}"></div>
                        <div class="post__body">
                       <p class="post__title">{{ $post->post_title }}</p>
                            <p class="post__link">czytaj więcej ></p>
                        </div>
                    </div>
                </a>
                </article>
            @endforeach
        </div>
    </div>
</section>
