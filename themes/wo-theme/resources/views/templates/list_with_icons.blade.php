@include('components.spacer')
<section class="t-lwi l-section l-borders">
    <div class="t-lwi__bg"></div>
    <div class="l-container">
        <div class="l-section__header">
            <h2 class="generic-title">{{ auto_nbsp($data['title']) }}</h2>
            <div class="generic-description">
                {!! auto_nbsp($data['description']) !!}
            </div>
        </div>

        @if (!empty($data['list']))
            <ul class="t-lwi__list">
                @foreach ($data['list'] as $listItem)

                    @if ($listItem['link'])
                        <li class="t-lwi__listItem">
                            <a class="t-lwi__listItem__link" href="{{$listItem['link']['url']}}">
                                <span class="imageWrapper">
                                    <img class="image" src="{{ $listItem['icon']['url'] }}" alt="">
                                </span>
                                <span class="text">{{ auto_nbsp($listItem['text']) }}</span>
                            </a>
                        </li>
                    @else
                        <li class="t-lwi__listItem">
                            <span class="imageWrapper">
                                <img class="image" src="{{ $listItem['icon']['url'] }}" alt="">
                            </span>
                            <span class="text">{{ auto_nbsp($listItem['text']) }}</span>
                        </li>
                    @endif


                @endforeach
            </ul>
        @endif


        @if (!empty($data['link']))
            <div class="t-lwi__buttonWrapper">
                <a href="{{ $data['link']['url'] }}" class="t-lwi__button a-button">{{ auto_nbsp($data['link']['title']) }}</a>
            </div>
        @endif

    </div>
</section>
