<div id="umow-porade-prawna" style="display:block;position:relative;top:-90px;pointer-events:none"></div>
<section class="t-bookVisit @if ($data['add_address_block']) --with-address @endif l-section no-spacer l-borders has-bg-lightblue">
    @if ($data['add_address_block'])
        <div class="l-container">
            <div class="t-bookVisit__col --col1">
                <div class="cms-content">
                    {!! auto_nbsp($data['address_block']) !!}
                </div>
            </div>
            <div class="t-bookVisit__col --col2">
                <div class="nf-form nf-form--visit">
                    {!! do_shortcode('[ninja_form id=2]') !!}
                </div>
            </div>
        </div>
    @else
        <div class="l-container">
            <div class="l-section__header">
                <h2 class="generic-title">{{ auto_nbsp($data['title']) }}</h2>
                <div class="generic-description">
                    {!! auto_nbsp($data['description']) !!}
                </div>
            </div>

            <div class="nf-form nf-form--visit">
                {!! do_shortcode('[ninja_form id=2]') !!}
            </div>
        </div>
    @endif
</section>
