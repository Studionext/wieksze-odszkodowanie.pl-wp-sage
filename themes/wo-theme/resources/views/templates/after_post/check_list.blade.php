<section class="t-checkList l-borders">
    <div class="l-container">
        <h2 class="t-checkList__title generic-title">{{ $data['title'] }}</h2>

        <ul class="t-checkList__list">
            @forelse($data['list'] as $item)
                <li class="t-checkList__listItem">
                    <svg fill="none" viewBox="0 0 24 19">
                        <path stroke="#0097FF" stroke-linecap="round" stroke-linejoin="round" stroke-opacity=".85" stroke-width="3" d="M2 10.766l5.818 6.2L22 1.636"/>
                    </svg> {{ $item['text'] }}</li>
            @empty
            @endforelse
        </ul>
    </div>
</section>

