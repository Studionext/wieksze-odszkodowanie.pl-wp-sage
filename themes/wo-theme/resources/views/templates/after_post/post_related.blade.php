<section class="t-related l-section l-borders">
    @include('components.spacer')
    <div class="l-container">
        <div class="l-section__header">
            <h2 class="generic-title">PODOBNE ARTYKUŁY</h2>
            <div class="generic-description">
             
            </div>
        </div>
@php
    $currentPostCategories= wp_get_post_categories($post->ID);
    $relatedDefault = get_posts([
        'category' => $currentPostCategories,
        'post__not_in' => [$post->ID],
        'numberposts'  => 4,
        'orderby' => 'rand'
    ]);
    $p = get_field('after_post');
    $related = ($p[1]['relatedPosts']);

    if (!empty($related)){
        $related = ($p[1]['relatedPosts']);
    }
    else{
        $related = $relatedDefault;
    }
@endphp
        @if (!empty($related))
            <ul class="t-media__list t-pressInfo__articles">
                @foreach ($related as $post)
                    <li class="m-pressTile t-pressInfo__article">
                        <a href="{{ get_permalink($post->ID) }}">
                             {{-- <div class="post__image lazyload" data-bg="{{ get_the_post_thumbnail_url($post -> ID) }}"> --}}
                             <div class="m-pressTile__image  no-image  lazyloaded" data-bg="@php echo get_the_post_thumbnail_url($post -> ID)  @endphp">
                            </div>
                            <div class="m-pressTile__body">
                                <p class="m-pressTile__title">
                                    @php 
                                    $str = get_the_title($post -> ID);
                                    echo mb_strimwidth($str, 0, 100, "...");
                                    @endphp
                                </p>
                                <div class="m-pressTile__link a-button --upper">Czytaj więcej ></div>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
</section>

