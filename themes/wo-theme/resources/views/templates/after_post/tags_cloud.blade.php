@php
// $tags = get_tags( array(
// 	'orderby' => 'name',
// 	'order'   => 'DESC',
// ) );
$tags = get_the_tags();
@endphp

@if (!empty($tags))
<section class="t-tags l-section l-borders">
	<div class="l-container">
    <h2 class="title">Tagi</h2>
        <ul class="list">
			@foreach ($tags as $tag)
			@php $tag_link = get_tag_link($tag);
			echo '<li class="list__item"><a href="'.esc_url( $tag_link ).'" title="'.esc_attr($tag->name).'">'.$tag->name.'</a></li>';
			@endphp
            @endforeach
        </ul>
    @endif
	</div>
</section>