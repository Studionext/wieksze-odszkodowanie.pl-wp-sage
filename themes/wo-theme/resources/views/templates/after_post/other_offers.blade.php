@php
    $posts = $data['posts'];

    if (empty($posts)) {
        $posts = get_posts([
            'numberposts' => 2,
            'post_parent' => 342,
            'post__not_in' => [$post->ID],
            'post_type' => 'page',
            'orderby' => 'rand'
        ]);
    }
    else {
        $posts = $data['posts'];
    }
@endphp

<section class="t-otherOffers l-section l-borders">
    <div class="l-container">
        <div class="l-section__header">
            <h2 class="generic-title">{{ auto_nbsp($data['title']) }}</h2>
        </div>

        <div class="t-otherOffers__posts">
            @if (!empty($posts))
                @foreach($posts as $post)
                    @if (empty($data['posts']))
                    @php
                    $image = get_the_post_thumbnail_url($post->ID);

                    if (empty($image)) {
                        $image = get_field('hero', $post->ID);

                        if (!empty($image)) {
                            $image = $image['images']['large']['url'];
                        }
                    }
                    @endphp
                    <article class="t-otherOffers__post post">
                        <div class="post__bg lazyload" data-bg="{{ $image }}"></div>
                        <a class="post__title--link" href="{{ get_permalink($post->ID)}}">
                        <h3 class="post__title">{{ $post->post_title }}</h3>
                        </a>
                        <a href="{{ get_permalink($post->ID)}}" class="post__button a-button --white">
                            Czytaj więcej
                        </a>
                    </article>
                    @else
                    @php
                    $image = get_the_post_thumbnail_url($post['post']['ID']);

                    if (empty($image)) {
                        $image = get_field('hero', $post['post']['ID']);

                        if (!empty($image)) {
                            $image = $image['images']['large']['url'];
                        }
                    }

                    @endphp
                    <article class="t-otherOffers__post post">
                        <div class="post__bg lazyload" data-bg="{{ $image }}"></div>
                        <a class="post__title--link" href="{{ get_permalink($post['post']['ID']) }}">
                        <h3 class="post__title">{{ $post['post']['post_title'] }}</h3>
                        </a>
                        <a href="{{ get_permalink($post['post']['ID']) }}" class="post__button a-button --white">
                            Czytaj więcej
                        </a>
                    </article>
                    @endif
                @endforeach
            @endif
        </div>

        @if (!empty($data['link']))
            <a class="t-otherOffers__button a-button --transparent" href="{{ $data['link']['url'] }}">{{ $data['link']['title'] }}</a>
        @endif
    </div>
</section>
