<section class="t-richList l-section l-borders has-bg-blue">
    @include('components.spacer')
    <div class="l-container">
        <div class="l-section__header">
            <h2 class="generic-title">{{ auto_nbsp($data['title']) }}</h2>
            <div class="generic-description">
                {!! auto_nbsp($data['description']) !!}
            </div>
        </div>

        @if (!empty($data['list']))
            <ul class="t-richList__list">
                @foreach ($data['list'] as $item)
                    <li class="t-richList__listItem item">
                        <div class="item__header">
                            <img data-src="{{ $item['icon']['url'] }}" alt="" class="lazyload image" src="{{ PX }}">
                            <div class="title">{{ auto_nbsp($item['title']) }}</div>
                        </div>
                        <div class="description">{{ auto_nbsp($item['description']) }}</div>
                        @if (!empty($item['link']))
                            <a href="{{ $item['link']['url'] }}" class="link">{{ $item['link']['title'] }} ></a>
                        @endif
                    </li>

                @endforeach
            </ul>
        @endif
    </div>
</section>
