<section class="l-borders l-section logos-slider-off">
    @include('components.spacer')
    <div class="l-container">
        <div class="l-section__header">
            <h2 class="generic-title">{{ auto_nbsp($data['title']) }}</h2>
            <div class="generic-description">
                {!! auto_nbsp($data['description']) !!}
            </div>
        </div>
        <div>
            <div class="a-fullWidthSlider js-slickInit" data-slick="fullWidth">
                @foreach ($globals['logos'] as $slide)
                <a class="custom-link" href="{{ $slide['logo-link']}}">
                    <img src="{{ $slide['image']['url'] }}" alt="{{$slide['logo-alt']}}">
                </a>
                @endforeach
            </div>
        </div>
    </div>
</section>