<section class="t-steps l-section l-borders">
    <div class="t-steps__bg"></div>
    @include('components.spacer')
    <div class="l-container">
        <div class="l-section__header">
            <h2 class="generic-title">{{ auto_nbsp($data['title']) }}</h2>
            <div class="generic-description">
                {!! auto_nbsp($data['description']) !!}
            </div>
        </div>

        @if (!empty($data['list']))
            <div class="t-steps__sliders">
                <div class="t-steps__navSlider js-stepsNav">
                    @foreach ($data['list'] as $key => $listItem)
                        <div data-step="{{ $key }}" class="t-steps__step {{$key === 0 ? 'is-active' : null}}">
                            <span class="number">{{ $key+1 }}.</span>
                            <span class="title">{{ auto_nbsp($listItem['title']) }}</span>
                        </div>
                    @endforeach
                </div>

                <div class="t-steps__contentSlider js-stepsContent">
                    @foreach ($data['list'] as $key => $listItem)
                        <div>
                            <div class="t-steps__content cms-content">
                                {{ auto_nbsp($listItem['content']) }}
                            </div>
                            @if (!empty($listItem['link']))
                                <a class="a-button --gray" href="{{ $listItem['link']['url'] }}">{{ auto_nbsp($listItem['link']['title']) }}</a>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        @endif

    </div>
</section>
