<section class="t-postRotator l-section l-borders">
    <div class="postRotator__bg"></div>
    <div class="l-container">
        @include('components.spacer')
        <div class="l-section__header">
            <h2 class="generic-title">{{ auto_nbsp($data['title']) }}</h2>
            <div class="generic-description">
                {!! auto_nbsp($data['description']) !!}
            </div>
        </div>


        @if (!empty($data['posts']))
            <div class="o-postRotator">


                @php
                    $firstPost = $data['posts'][0]['post'];
                    $img = get_the_post_thumbnail_url($firstPost['ID']);
                @endphp

                <div class="js-postRotator o-postRotator">
                    <div class="o-postRotator__col --col1">
                        <div class="js-img o-postRotator__active lazyload" data-bg="{{ $img }}"></div>
                    </div>
                    <div class="o-postRotator__col --col2">
                        <ul class="o-postRotator__list">
                            @foreach ($data['posts'] as $key => $post)
                                <li class="o-postRotator__listItem">
                                    <button data-key="{{ $key }}">
                                        <img class="lazyload" src={{ PX }} data-src="{{ get_the_post_thumbnail_url($post['post']['ID']) }}" alt="{{ $post['post']['post_title'] }}">
                                    </button>

                                    <div style="display: none;">
                                        <div data-item="{{ $key }}" class="o-postRotator__content">
                                            <div data-img="{{ get_the_post_thumbnail_url($post['post']['ID']) }}"></div>
                                            <div data-item-content>
                                                <h3 class="o-postRotator__title">
                                                    {{ auto_nbsp($post['post']['post_title']) }}
                                                </h3>
                                        
                                                <div class="o-postRotator__description">
                                                    {{ auto_nbsp(wp_trim_words($post['post']['post_content'], 20)) }}
                                                </div>
                                                
                                                <a href="{{get_permalink($post['post']['ID'])}}" class="o-postRotator__link">Czytaj więcej ></a>
                                            </div>
                                        </div>
                                    </div>

                                </li>

                            @endforeach
                        </ul>
                        <div class="js-content o-postRotator__content">
                            <h3 class="js-title o-postRotator__title">
                                {{ auto_nbsp($firstPost['post_title']) }}
                            </h3>
                            <div class="js-desc o-postRotator__description">
                                {{ auto_nbsp(wp_trim_words($firstPost['post_content'], 20)) }}
                            </div>
                            <a href="{{get_permalink($firstPost['ID'])}}" class="js-link o-postRotator__link">Czytaj więcej ></a>
                        </div>
                    </div>
                </div>

            </div>
        @endif

    </div>
</section>
