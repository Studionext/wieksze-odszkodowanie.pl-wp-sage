<section class="t-richList --highlights l-section l-borders has-bg-blue">
    <div class="l-container">
        @if (!empty($data['title']) || !empty($data['description']))
            @include('components.spacer')
            <div class="l-section__header">
                <h2 class="generic-title">{{ auto_nbsp($data['title']) }}</h2>
                <div class="generic-description">
                    {!! auto_nbsp($data['description']) !!}
                </div>
            </div>

        @endif

        @if (!empty($data['list']))
            <ul class="t-richList__list">
                @foreach ($data['list'] as $item)
                    <li class="t-richList__listItem">
                        <div class="header">
                            <img data-src="{{ $item['icon']['url'] }}" alt="" class="lazyload image" src="{{ PX }}">
                            <div class="title">{{ auto_nbsp($item['title']) }}</div>
                        </div>
                        <div class="description">{{ auto_nbsp($item['description']) }}</div>
                        @if (!empty($item['link']))
                            <a href="{{ $item['link']['url'] }}" class="link">{{ $item['link']['title'] }} ></a>
                        @endif
                    </li>
                @endforeach
            </ul>
            @if (!empty($data['themostlink']))
            <div class="t-richList__buttonWrapper">
                <a href="{{ $data['themostlink']['url'] }}" class="a-button --gray">{{ auto_nbsp($data['themostlink']['title']) }}</a>
            </div>
        @endif
        @endif
    </div>
</section>
