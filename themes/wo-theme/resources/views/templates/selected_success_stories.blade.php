<section class="t-3posts l-section l-borders">
    <div class="t-3posts__bg"></div>
    <div class="l-container">
        @include('components.spacer')
        <div class="l-section__header">
            <h2 class="generic-title">{{ auto_nbsp($data['title']) }}</h2>
            <div class="generic-description">
                {!! auto_nbsp($data['description']) !!}
            </div>
        </div>

        <div class="t-3posts__posts">
            @if (!empty($data['posts']))
                @foreach($data['posts'] as $post)
                    <article class="t-3posts__post post">
                        <a href="{{ get_permalink($post['post']['ID']) }}">
                            <p class="post__title">
                                <span class="amount">{{ $post['title'] }}</span>
                                <br>
                                <span class="text">{{ auto_nbsp($post['description']) }}</span>
                            </p>
                            <div class="post__button a-button --white">
                                Czytaj więcej
                            </div>
                        </a>
                    </article>
                @endforeach
            @endif
        </div>

        @if (!empty($data['link']))
            <a class="t-3posts__button a-button --transparent" href="{{ $data['link']['url'] }}">{{ $data['link']['title'] }}</a>
        @endif
    </div>
</section>
