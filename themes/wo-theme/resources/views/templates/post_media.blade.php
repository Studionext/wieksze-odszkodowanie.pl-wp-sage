<section class="t-media l-section l-borders">
    @include('components.spacer')
    <div class="l-container">
        <div class="l-section__header">
            <h2 class="generic-title">{{ auto_nbsp($data['title']) }}</h2>
            <div class="generic-description">
                {!! auto_nbsp($data['description']) !!}
            </div>
        </div>

        @if (!empty($data['posts']))
            <ul class="t-media__list">
                @foreach ($data['posts'] as $post)

                    <li class="t-media__post post">
                        <a href="{{ get_permalink($post['post']['ID']) }}">
                            <div class="post__image lazyload" data-bg="{{ get_the_post_thumbnail_url($post['post']['ID']) }}">

                            </div>
                            <p class="post__title">
                                @if ($post['title']) {{ $post['title'] }} @else {{ $post['post']['post_title'] }} @endif
                            </p>
                            @if ($post['name'])
                                <p class="post__auditionName">{{ $post['name'] }}</p>
                            @endif
                        </a>
                    </li>
                @endforeach
            </ul>
        @endif

    </div>
</section>

