@extends('layouts.default')

@section('main')
<div class="l-offsetHeader"></div>

@if (!isset($hero) || empty($hero))
@php
$hero = (object) [
'images' => (object) [
'small' => (object) [
'url' => get_template_directory_uri().'dist/image/image.jpg'
],
'large' => (object) [
'url' => get_template_directory_uri().'dist/image/image.jpg'
]
]
];
@endphp
@endif
@if (is_single())

@php

/* DIRTY HAX FOR SINGLE HERO */
$hero = (object) [
'images' => (object) [
'small' => (object) [
'url' => get_the_post_thumbnail_url()
],
'large' => (object) [
'url' => get_the_post_thumbnail_url()
]
]
];
@endphp
@endif


@include('parts.common.singleHero', ['data' => $hero])

@include('parts.common.breadcrumbs')


<section class="l-section t-withSidebar l-borders">
    <div class="l-container">
        <div class="t-withSidebar__col --col1">
            <div class="cms-content">
                {{ the_content() }}
            </div>
        </div>

        <div class="t-withSidebar__col --col2">
            @include('parts.common.asideCard')
        </div>

    </div>
</section>




@if (!empty($after_post))
@foreach ($after_post as $template)
@include("templates.after_post.$template->acf_fc_layout", ['data' =>
\App\Controllers\App::parseTemplateData($template)])
@endforeach
@endif

@if (!empty($templates))
@foreach ($templates as $template)
@include("templates.$template->acf_fc_layout", ['data' => \App\Controllers\App::parseTemplateData($template)])
@endforeach
@endif

@endsection