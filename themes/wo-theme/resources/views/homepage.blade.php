{{--
Template name: Homepage
--}}

@extends('layouts.default')

@section('main')
    <div class="l-offsetHeader"></div>
    @include('parts.homepage.hero', ['data' => $hero])
    @include('parts.common.logosSlider', ['data' => $globals['logos']])
    @include('parts.common.textBlocksWithImage', ['data' => $text_section])

    @foreach ($templates as $template)
        @include("templates.$template->acf_fc_layout", ['data' => \App\Controllers\App::parseTemplateData($template)])
    @endforeach

@endsection
