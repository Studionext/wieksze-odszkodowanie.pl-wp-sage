{{--
Template name: Press info
--}}

@extends('layouts.default')

@section('main')
    <div class="l-offsetHeader"></div>
    @include('parts.common.singleHero', ['data' => $hero])
    @include('parts.common.breadcrumbs')

    <section class="t-pressInfo l-section l-borders">
        <div class="l-container">
            @if (!empty($page_lead))
                <h2 class="t-pressInfo__lead">
                    {{ $page_lead }}
                </h2>
            @endif

            @if (!empty($posts))
                <div class="t-pressInfo__articles">
                    @foreach ($posts as $post)
                        @php
                            $image = get_the_post_thumbnail_url($post->ID);
                        @endphp
                        <div class="t-pressInfo__article">
                            <a href="{{ get_permalink($post->ID) }}">
                                <article class="m-pressTile">
                                    <div class="m-pressTile__image @if (!$image) no-image @endif lazyload" data-bg="{{ $image }}"></div>
                                    <div class="m-pressTile__body">
                                        <h3 class="m-pressTile__title">{{ $post->post_title }}</h3>
                                        <div class="m-pressTile__description">
                                            {!! get_the_excerpt($post->ID) !!}
                                        </div>
                                        <div class="m-pressTile__link a-button --upper">Czytaj więcej ></div>
                                    </div>
                                </article>
                            </a>
                        </div>
                    @endforeach
                </div>
            @else
                <h3>Niestety, nie mamy żadnych artykułów</h3>
            @endif

        </div>
    </section>

    @if (!empty($templates))
        @foreach ($templates as $template)
            @include("templates.$template->acf_fc_layout", ['data' => \App\Controllers\App::parseTemplateData($template)])
        @endforeach
    @endif

@endsection
