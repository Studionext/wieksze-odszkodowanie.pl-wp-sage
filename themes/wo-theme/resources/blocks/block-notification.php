<div class="a-callout <?= block_field('background-color'); ?>">
    <h3 class="a-callout__title"><?= block_field('title') ?></h3>
    <div class="a-callout__description">
        <?= auto_nbsp(block_field('description')); ?>
    </div>
</div>

