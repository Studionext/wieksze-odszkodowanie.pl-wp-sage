wp.domReady(() => {
  wp.blocks.registerBlockStyle("core/list", {
    name: "default",
    label: "Domyślna lista",
    isDefault: true,
  });

  wp.blocks.registerBlockStyle("core/list", {
    name: "check-icon",
    label: "Ikona 'ptaszek'",
  });

  wp.blocks.registerBlockStyle("core/list", {
    name: "chevron-icon",
    label: "Ikona 'strzałka'",
  });

  wp.blocks.registerBlockStyle("core/list", {
    name: "big-bullet",
    label: "Ikona 'niebieskie kółko'",
  });

  wp.blocks.registerBlockStyle("core/list", {
    name: "big-numbers",
    label: "Duże numery (tylko lista numerowana)",
  });
});
