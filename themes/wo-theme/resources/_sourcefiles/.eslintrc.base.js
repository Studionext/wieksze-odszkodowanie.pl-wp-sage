/*
 * ESLint global settings go here.
 * For project specific rules - please see .eslintrc.js
 * */

module.exports = {
  env: {
    browser: true,
    es6: true,
    commonjs: true,
    jquery: true
  },
  extends: ['airbnb-base', 'plugin:prettier/recommended'],
  plugins: ['prettier'],
  globals: {},
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 6
  },
  rules: {
    'import/prefer-default-export': 'off',
    'import/extensions': 'off',
    'import/no-extraneous-dependencies': 'off',
    'no-console': 'off'
  }
};
