/*
 * Don't try to run this.
 * It won't work for now.
 *
 * @TODO Refactor and make runable.
 * */
const path = require('path');
const fs = require('fs-extra');
const webpack = require('webpack');
const browserSync = require('browser-sync').create();
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackConfig = require('./webpack.config')({
  serveMethod: 'browserSync'
});
const paths = require('./paths');
const project = require('../project.config');

const compiler = webpack(webpackConfig);

const imgSrc = path.resolve(project.context, 'img');
const imgOutput = path.resolve(project.buildPath, 'img');

fs.removeSync(project.buildPath);
if (imgSrc) {
  fs.copy(imgSrc, imgOutput);

  /*
   *  Watch for file changes in img folder.
   *  Rebuild if change (new/deleted).
   * */
  fs.watch(imgSrc, () => {
    fs.removeSync(imgOutput);
    fs.copy(imgSrc, imgOutput);
  });
}

const middleware = [
  webpackDevMiddleware(compiler, {
    publicPath: `/${paths.wdsPath}`,
    logLevel: 'silent',
    quiet: true
  }),
  webpackHotMiddleware(compiler, {
    log: false,
    logLevel: 'none'
  })
];

fs.removeSync(`${path.resolve(__dirname, '..', '..')}/.dev`);
fs.appendFile(`${path.resolve(__dirname, '..', '..')}/.dev`, `browsersync`);

browserSync.init({
  middleware,
  open: false,
  proxy: {
    target: project.browserSync.proxyTarget,
    middleware
  },
  logLevel: 'info',
  files: project.watch.map((element) => path.resolve(element)),
  snippetOptions: {
    rule: {
      match: /<\/head>/i,
      fn: function (snippet, match) {
        return `
            <script defer="defer" src="/${paths.wdsPath}js/main.js"></script>
            ${snippet}${match}
        `;
      }
    }
  },
  rewriteRules: []
});

/* @TODO move this to somewhere else
 *  this part deletes the lockfile on process end
 * */
// so the program will not close instantly
process.stdin.resume();

function exitHandler(options, exitCode) {
  fs.removeSync(`${path.resolve(__dirname, '..', '..')}/.dev`);
  process.exit();
}

// app close
process.on('exit', exitHandler.bind(null, { cleanup: true }));

// catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, { exit: true }));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, { exit: true }));
process.on('SIGUSR2', exitHandler.bind(null, { exit: true }));

// catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, { exit: true }));
