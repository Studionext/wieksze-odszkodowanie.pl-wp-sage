const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const { argv } = require('yargs');
const pluginsProduction = require('./partials/plugins/production');
const pluginsDevelopment = require('./partials/plugins/development');
const rules = require('./partials/rules/all');
const paths = require('./paths');
const project = require('../project.config');
const hmr = require('./hmr/index');

module.exports = ({ serveMethod }) => {
  const { production, pretty } = argv;

  console.log('Production: ', production || false);
  console.log('Pretty: ', pretty || false);

  //@TODO handle this better.
  const { entryPoints } = project;
  let publicPath = `http://localhost:${project.wdsPort}/${paths.wdsPath}`;

  if (serveMethod === 'browserSync') {
    for (const [key, value] of Object.entries(entryPoints)) {
      entryPoints[key] = [hmr.getClient(), value];
    }

    publicPath = `http://localhost:${project.browserSync.port}/${paths.wdsPath}`;
  }

  return {
    context: project.context,
    entry: entryPoints,
    mode: production ? 'production' : 'development',
    output: {
      filename: production && !pretty ? '[name].[hash].min.js' : '[name].js',
      path: project.buildPath,
      publicPath: production ? project.publicPath : publicPath,
      chunkFilename:
        production && !pretty
          ? 'js/[name].[hash].min.async.js'
          : 'js/[name].[hash].async.js'
    },
    externals: {
      jquery: 'jQuery'
    },
    devtool: production && !pretty ? false : 'source-map',
    resolve: {
      alias: {
        fonts: path.resolve(project.context, 'fonts'),
        img: path.resolve(project.context, 'img'),
        vendor: path.resolve(project.context, 'vendor'),
        video: path.resolve(project.context, 'video')
      }
    },
    optimization: {
      minimizer:
        production && !pretty
          ? [
              new TerserPlugin({
                terserOptions: {
                  compress: {
                    drop_console: true
                  }
                }
              })
            ]
          : [],
      splitChunks: {}
    },
    module: {
      rules: [rules.script, rules.style, rules.font, rules.image]
    },
    plugins: production ? [...pluginsProduction] : [...pluginsDevelopment]
  };
};
