const scriptRules = require('./scripts');
const styleRules = require('./styles');
const fontRules = require('./fonts');
const imageRules = require('./images');

const rules = {
  script: scriptRules,
  style: styleRules,
  font: fontRules,
  image: imageRules
};

module.exports = rules;
