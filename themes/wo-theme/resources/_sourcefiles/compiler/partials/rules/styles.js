const { argv } = require('yargs');
const MiniCssExtractWebpackPlugin = require('mini-css-extract-plugin');

const { production, pretty } = argv;

const styleRules = {
  test: /\.(sa|sc|c)ss$/,
  use: [
    ...(production
      ? [MiniCssExtractWebpackPlugin.loader]
      : [{ loader: 'cache-loader' }, { loader: 'style-loader' }]),
    { loader: 'css-loader', options: { sourceMap: true } },
    ...(production
      ? [
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              config: { ctx: { production, pretty } },
              sourceMap: true
            }
          }
        ]
      : []),
    { loader: 'resolve-url-loader', options: { sourceMap: true } },
    {
      loader: 'sass-loader',
      options: {
        sassOptions: {},
        sourceMap: true
      }
    }
  ]
};

module.exports = styleRules;
