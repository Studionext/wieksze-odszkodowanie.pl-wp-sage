const fontRules = {
  test: /\.(ttf|otf|eot|woff2?)$/,
  use: [
    {
      loader: 'file-loader',
      options: {
        name: 'fonts/[name].[ext]'
      }
    }
  ]
};

module.exports = fontRules;
