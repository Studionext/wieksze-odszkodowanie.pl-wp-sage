const { argv } = require('yargs');

const { production } = argv;

const scriptRules = {
  test: /\.(js|jsx)$/,
  exclude: /(node_modules|bower_components)/,
  use: [
    ...(production ? [] : [{ loader: 'cache-loader' }]),
    { loader: 'babel-loader' }
  ]
};

module.exports = scriptRules;
