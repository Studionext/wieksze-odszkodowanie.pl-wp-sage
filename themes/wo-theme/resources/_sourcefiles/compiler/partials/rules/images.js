const imageRules = {
  test: /\.(png|jpe?g|gif|svg|ico|webm)$/,
  use: [
    {
      loader: 'url-loader',
      options: {
        limit: 8192,
        name: 'img/[name].[ext]'
      }
    }
  ]
};

module.exports = imageRules;
