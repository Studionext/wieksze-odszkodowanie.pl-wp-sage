const webpack = require('webpack');
const MiniCssExtractWebpackPlugin = require('mini-css-extract-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const imageminMozjpeg = require('imagemin-mozjpeg');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { argv } = require('yargs');

const NonJsEntryCleanupPlugin = require('../../non-js-entry-cleanup-plugin');

const { production, pretty } = argv;
const project = require('../../../project.config');

const pluginsProduction = [
  new ManifestPlugin(),
  new MiniCssExtractWebpackPlugin({
    moduleFilename: ({ name }) => {
      const suffix = production && !pretty ? '.[hash].min.css' : '.css';
      return `${name.replace('js/', 'css/')}${suffix}`;
    },
    chunkFilename:
      production && !pretty ? 'css/[name].[hash].min.css' : 'css/[name].css'
  }),
  new NonJsEntryCleanupPlugin({
    context: 'css',
    extensions: 'js',
    includeSubfolders: true
  }),
  new CopyWebpackPlugin({
    patterns: [
      {
        from: '**/*',
        to: project.buildPath,
        context: project.context,
        globOptions: {
          ignore: [
            '**/*.js',
            '**/*.ts',
            '**/*.sass',
            '**/*.scss',
            '**/*.css',
            '**/.gitkeep'
          ]
        },
        noErrorOnMissing: true
      }
    ]
  }),
  ...(project.compressImages
    ? [
        new ImageminPlugin({
          pngquant: { quality: '80-80' },
          plugins: [imageminMozjpeg({ quality: 73 })]
        })
      ]
    : []),
  ...(project.cleanBuildPathBeforeBuild ? [new CleanWebpackPlugin()] : []),
  new webpack.DefinePlugin({
    webpackMode: JSON.stringify('production')
  })
];

module.exports = pluginsProduction;
