const webpack = require('webpack');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');

const pluginsDevelopment = [
  new webpack.HotModuleReplacementPlugin(),
  new FriendlyErrorsWebpackPlugin(),
  new webpack.DefinePlugin({
    webpackMode: JSON.stringify('development')
  })
];

module.exports = pluginsDevelopment;
