const path = require('path');
const minimatch = require('minimatch');

module.exports = class {
  constructor(options) {
    this.options = options;
  }

  apply(compiler) {
    const { context, extensions, includeSubfolders } = this.options;

    compiler.hooks.emit.tapAsync(
      'NonJsEntryCleanupPlugin',
      (compilation, callback) => {
        const assetPath = path.join(
          context,
          `${includeSubfolders ? '**/' : '/'}`
        );

        const pattern = `${assetPath}*.+(js|js.map)`;

        Object.keys(compilation.assets)
          .filter((asset) => minimatch(asset, pattern))
          .forEach((asset) => delete compilation.assets[asset]);

        callback();
      }
    );
  }
};
