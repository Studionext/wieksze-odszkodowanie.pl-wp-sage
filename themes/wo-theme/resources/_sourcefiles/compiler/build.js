const fs = require('fs-extra');
const path = require('path');
const webpack = require('webpack');
const project = require('../project.config');
const config = require('./webpack.config')(process.env, process.argv);

fs.removeSync(`${path.resolve(__dirname, '..', '..')}/.dev`);

webpack(config, (err, stats) => {
  if (err) {
    console.error(err.stack || err);
    if (err.details) {
      console.error(err.details);
    }

    fs.removeSync(project.buildPath);
    return;
  }

  if (stats.hasErrors()) {
    console.error(
      stats.toString({
        all: false,
        colors: true,
        errors: true
      })
    );

    fs.removeSync(project.buildPath);
    return;
  }

  if (stats.hasWarnings()) {
    console.warn(
      stats.toString({
        all: false,
        colors: true,
        errors: true
      })
    );
  }

  console.log(
    stats.toString({
      colors: true,
      chunks: false,
      modules: false,
      entrypoints: false,
      children: false
    })
  );
});
