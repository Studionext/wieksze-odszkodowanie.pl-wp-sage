const path = require('path');
const fs = require('fs-extra');
const chokidar = require('chokidar');
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');

const paths = require('./paths');
const project = require('../project.config');
const config = require('./webpack.config')({
  serveMethod: 'wds'
});

/*
 * Files from these dev-assets folders will be copied to build folder during development.
 * This is sometimes needed, ie. when using images directly in PHP - webpack has no knowledge of this.
 * */
const devAssetsToWatch = ['img', 'video'];

const compiler = webpack(config);

const server = new WebpackDevServer(compiler, {
  hot: true,
  quiet: true,
  overlay: true,
  liveReload: true,
  port: project.wdsPort,
  publicPath: `/${paths.wdsPath}`,
  disableHostCheck: true,
  writeToDisk: false,
  watchOptions: {
    poll: 1000,
    ignored: ['node_modules']
  },
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
    'Access-Control-Allow-Headers':
      'X-Requested-With, content-type, Authorization'
  },
  before: (app, srv) => {
    const root = `${project.watchRoot}/`;
    const files = project.watch.map((el) => root + el);

    console.log(`Watch root: ${root}`);
    console.log('Watching files: ');
    console.log(files);

    chokidar
      .watch(files, {
        alwaysStat: true,
        atomic: false,
        followSymlinks: false,
        ignoreInitial: true,
        ignorePermissionErrors: true,
        persistent: true,
        usePolling: true
      })
      .on('all', () => {
        srv.sockWrite(srv.sockets, 'content-changed');
      });
  }
});

// Delete all existing build assets.
fs.removeSync(project.buildPath);

const parsed = devAssetsToWatch.map((el) => {
  return {
    name: el,
    path: `${project.context}/${el}`
  };
});

parsed.forEach((el) => {
  if (!fs.existsSync(el.path)) {
    return;
  }

  fs.readdir(el.path, (err, files) => {
    if (err) {
      console.log('err');
      console.log(err);
    } else if (files.length === 1 && files.includes('.gitkeep')) {
      console.log(`No files to copy from ${el.name}...`);
    } else {
      console.log(`Copying files from ${el.name}...`);

      fs.copy(el.path, `${project.buildPath}/${el.name}`);
    }
  });

  // @TODO copy only new files instead of whole dir
  fs.watch(el.path, () => {
    console.log(`Copying files from ${el.name}...`);
    fs.removeSync(`${project.buildPath}/${el.name}`);
    fs.copySync(el.path, `${project.buildPath}/${el.name}`);
  });
});

server.listen(project.wdsPort, '', () => {
  console.log(`| -------------------------------------------- |`);
  console.log(
    `| Webpack dev server is running on http://localhost:${project.wdsPort} |`
  );
  console.log(`| Have fun developing! 🔥                      |`);
  console.log(`| -------------------------------------------- |`);
  console.log(``);
  console.log(`Preparing assets...`);

  fs.removeSync(`${path.resolve(__dirname, '..', '..')}/.dev`);
  fs.appendFile(
    `${path.resolve(__dirname, '..', '..')}/.dev`,
    `${project.wdsPort}|${paths.wdsPath}`
  );
});

/* @TODO move this to somewhere else
 *  this part deletes the lockfile on process end
 * */
// so the program will not close instantly
process.stdin.resume();

function exitHandler(options, exitCode) {
  fs.removeSync(`${path.resolve(__dirname, '..', '..')}/.dev`);
  server.close();
  process.exit();
}

// app close
process.on('exit', exitHandler.bind(null, { cleanup: true }));

// catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, { exit: true }));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, { exit: true }));
process.on('SIGUSR2', exitHandler.bind(null, { exit: true }));

// catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, { exit: true }));
