/*
 * Paths used by Webpack and Webpack-dev-server
 * Please don't change anything here,
 * unless you know what you are doing
 * */

module.exports = {
  // path for webpack-dev-server to place assets into
  wdsPath: '__wds__/'
};
