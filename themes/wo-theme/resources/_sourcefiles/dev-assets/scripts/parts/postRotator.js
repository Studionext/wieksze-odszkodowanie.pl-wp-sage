export const initPostRotator = () => {
  const $el = $('.js-postRotator');
  const $img = $el.find('.js-img');
  const $content = $el.find('.js-content');

  let currentPost = 0;

  const showSlide = (slide) => {
    const $slide = $(`[data-item="${slide}"]`);

    $img.fadeOut('fast', function () {
      const imgUrl = $slide.find('[data-img]').attr('data-img');

      console.log(imgUrl);

      $img.css({
        backgroundImage: `url("${imgUrl}")`
      });

      $img.fadeIn('fast');
    });

    $content.fadeOut('fast', function () {
      $content.children().remove();
      $content.append($slide.find('[data-item-content]').children().clone());

      $content.fadeIn('fast');
    });
  };

  $el.on('click', 'button[data-key]', function () {
    showSlide($(this).attr('data-key'));
  });
};
