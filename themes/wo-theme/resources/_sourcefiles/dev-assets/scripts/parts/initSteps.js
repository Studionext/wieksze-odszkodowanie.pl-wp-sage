import 'slick-carousel';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';

export const initSteps = () => {
  const $nav = $('.js-stepsNav');
  const $content = $('.js-stepsContent');

  const SLIDER_BREAKPOINT = 1100;

  if ($(window).width() < SLIDER_BREAKPOINT) {
    $nav.slick({
      variableWidth: true,
      focusOnSelect: true,
      asNavFor: '.js-stepsContent'
    });
  } else {
    $nav.on('click', '[data-step]', function () {
      const key = $(this).attr('data-step');
      $('[data-step]').removeClass('is-active');
      $(this).addClass('is-active');
      $content.slick('slickGoTo', key);
    });
  }

  $nav.on('beforeChange', function (e, slick) {
    $(slick.$slider).find('.is-active').removeClass('is-active');
  });

  const options = {
    fade: true,
    adaptiveHeight: true,
    arrows: false,
    draggable: false
  };

  if ($(window).width() < SLIDER_BREAKPOINT) {
    options.asNavFor = '.js-stepsNav';
  }

  $content.slick(options);
};
