export const initSuccessStories = () => {
    const $wrapper = $('.js-successStories');

    if (!$wrapper.length) {
        return;
    }

    $('.js-successTile').each(function() {
        const $tile = $(this);

        const min = $tile.attr('data-min');
        const max = $tile.attr('data-max');

        const total = parseInt(min, 10) + parseInt(max, 10);

        if (isNaN(total)) {
            return;
        }

        const percentMin = (min / total) * 100;
        const percentMax = 100 - percentMin;

        $tile.find('[data-transparent]').width(percentMin + '%');
        $tile.find('[data-shield]').css({ left: percentMin + '%' });
        $tile.find('[data-color]').width(percentMax + '%');
    });
};