const _t = require('lodash.throttle');

const throttled = _t(function() {
    const scrollPos = $(window).scrollTop();

    if (scrollPos > 260) {
        if ($('.js-stickyDesktop').hasClass('is-shown')) {
            return;
        }

        $('.js-stickyDesktop').addClass('is-visible');

        setTimeout(() => {
            $('.js-stickyDesktop').addClass('is-shown');
        }, 500);
    } else {
        $('.js-stickyDesktop').removeClass('is-visible');
        $('.js-stickyDesktop').removeClass('is-shown');
    }
}, 50);

export const stickyDesktopBar = () => {
    $(window).on('scroll', throttled);

    $('.js-desktop-burger').on('click', () => {
        $('.js-desktop-burger').toggleClass('is-active');
        $('.js-desktop-nav').toggle();
    });
};

$('.js-burger-category').on('click', () => {
    $('.js-burger-category').toggleClass('is-active');
    $('.m-blogMenu').toggleClass('is-active');
});