import 'slick-carousel';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';

const settings = {
    default: {
        infinite: true,
        touchThreshold: 20,
        variableWidth: true,
        autoplay: true,
        arrows: false,
        swipeToSlide: true,
    },
    fullWidth: {
        infinite: true,
        slidesToScroll: 1,
        autoplay: true,
        centerMode: true,
        centerPadding: '20px',
        variableWidth: true,
        arrows: false,
        touchThreshold: 22,
        swipeToSlide: true,
    },
    whyUs: {
        arrows: false,
        infinite: true,
        slidesToScroll: 1,
        slidesToShow: 2,
        variableWidth: true,
        touchThreshold: 22,
        swipeToSlide: true,
        responsive: [{
                breakpoint: 1025,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    },
    blocksSlider: {
        arrows: false,
        slidesToShow: 3,
        centerMode: true,
        touchThreshold: 22,
        swipeToSlide: true,
        slidesToScroll: 2,
        responsive: [{
                breakpoint: 1025,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    }
};

export const initCarousels = () => {
    $('.js-slickInit').each(function() {
        const initSettings = $(this).attr('data-slick');

        let options = settings.default;

        if (initSettings) {
            options = settings[initSettings];
        }

        $(this).slick(options);
    });

    $('.js-whyUsNav').click(function() {
        const dir = $(this).attr('data-dir');

        if (dir === 'next') {
            $('[data-slick="whyUs"]').slick('slickNext');
        } else {
            $('[data-slick="whyUs"]').slick('slickPrev');
        }
    });
};