import { initCarousels } from './parts/initCarousels';
import { initSteps } from './parts/initSteps';
import { stickyDesktopBar } from './parts/stickyDesktopBar';
import { initPostRotator } from './parts/postRotator';
import { initSuccessStories } from './parts/initSuccessStories';

const { Nav } = require('./classes/Nav');

$(document).ready(() => {
  stickyDesktopBar();
  new Nav();
  initPostRotator();
  initSuccessStories();
});

$(window).on('load', () => {
  initCarousels();
  initSteps();
});


var stories = $('.t-successStories__article');
// stories.hide();


$( "#success-category-menu" ).change(function() { 

    var categoryType = $(this).children("option:selected").val();
    
	console.log(categoryType);
    if(categoryType == 69 || categoryType == 70 || categoryType == 71 || categoryType == 72 || categoryType == 73){
       stories
        .hide()
        .filter(function () {
            return $(this).data('category') == categoryType;
        })
        .show();
    }
    else{

      stories.show();
    }

   
});