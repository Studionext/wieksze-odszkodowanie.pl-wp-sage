import 'lazysizes';
import 'lazysizes/plugins/unveilhooks/ls.unveilhooks';

console.log('critical.js');

/*
 * Set up Intersection Observers
 * We have two of them (using different rootMargins):
 * 1. viewportObserver - for animations and similar stuff
 * 2. visibilityObserver - keeping track of currently visible elements
 * 3. moduleObserver - for initiating async JS modules load
 * */
const viewportObserverOptions = {
  rootMargin: '0% 0% -20% 0%',
  threshold: [0]
};

const viewportObserverCb = (entries) => {
  entries.forEach((entry) => {
    if (entry.isIntersecting) {
      entry.target.classList.add('in-viewport');

      if (entry.target.classList.contains('a-slideInWrapper')) {
        entry.target
          .querySelector('.a-slideIn')
          .style.setProperty('--h', entry.target.clientHeight + 'px');
      }

      // eslint-disable-next-line no-use-before-define
      viewportObserver.unobserve(entry.target);
    }
  });
};

const viewportObserver = new IntersectionObserver(
  viewportObserverCb,
  viewportObserverOptions
);

const visibilityObserverOptions = {
  rootMargin: '50% 0% 50% 0%',
  threshold: [0]
};

const visibilityObserverCb = (entries) => {
  entries.forEach((entry) => {
    console.log(entry);
    if (entry.isIntersecting) {
      entry.target.classList.add('is-visible');
    } else {
      entry.target.classList.remove('is-visible');
    }
  });
};

const visibilityObserver = new IntersectionObserver(
  visibilityObserverCb,
  visibilityObserverOptions
);

const moduleObserverOptions = {
  rootMargin: '0% 0% 100% 0%',
  threshold: [0]
};

const moduleObserverCb = (entries) => {
  entries.forEach((entry) => {
    if (entry.isIntersecting) {
      const element = entry.target;
      const moduleName = element.dataset.asyncModule;

      if (moduleName) {
        const event = new CustomEvent(`loadModule`, {
          bubbles: true,
          detail: { module: moduleName }
        });

        element.dispatchEvent(event);
      }

      element.classList.add('loaded');

      // eslint-disable-next-line no-use-before-define
      moduleObserver.unobserve(entry.target);
    }
  });
};

const moduleObserver = new IntersectionObserver(
  moduleObserverCb,
  moduleObserverOptions
);

/*
 * This script is loaded async.
 * First we fire ASAP, then, we wait until DOMContentLoaded
 * to add elements, which might be missed on the first time.
 * */
const viewportElements = document.querySelectorAll('.--observe');
viewportElements.forEach((el) => viewportObserver.observe(el));

const visibilityElements = document.querySelectorAll('.--observe-visibility');
visibilityElements.forEach((el) => visibilityObserver.observe(el));

const moduleElements = document.querySelectorAll('[data-async-module]');
moduleElements.forEach((el) => moduleObserver.observe(el));

document.addEventListener('DOMContentLoaded', () => {
  // eslint-disable-next-line no-shadow
  const viewportElements = document.querySelectorAll('.--observe');
  viewportElements.forEach((el) => viewportObserver.observe(el));

  // eslint-disable-next-line no-shadow
  const visibilityElements = document.querySelectorAll('.--observe-visibility');
  visibilityElements.forEach((el) => visibilityObserver.observe(el));

  // eslint-disable-next-line no-shadow
  const moduleElements = document.querySelectorAll('[data-async-module]');
  moduleElements.forEach((el) => moduleObserver.observe(el));
});

/*
 * Lazysizes set up
 * */
const getCurrentBreakpoint = () => {
  return (
    window
      .getComputedStyle(document.documentElement)
      // eslint-disable-next-line no-useless-escape
      .getPropertyValue('--current-breakpoint')
      .replace(/"/g, '')
  );
};

const order = ['sm', 'md', 'lg', 'xl', 'xxl', 'wide'];

/*
 * We're using lazysizes hook to calculate what image size should be
 * lazyloaded, based on current viewport.
 * We're trying to load the most fitting image,
 * and when it's not available - load the closest available.
 * */
document.addEventListener('lazybeforeunveil', function (e) {
  const currentBreakpoint = getCurrentBreakpoint();
  const attributes = Array.from(e.target.attributes).map((attr) => attr.name);
  const regExp = new RegExp('data-bg', 'g');
  let bg;

  // Bail if no data-bg attribute
  if (!attributes.some((attr) => regExp.test(attr))) {
    return;
  }

  // If there is a matching attribute for current breakpoint - get it
  if (attributes.includes(`data-bg-${currentBreakpoint}`)) {
    bg = e.target.getAttribute(`data-bg-${currentBreakpoint}`);
  }

  // else, try to get the highest possible data-bg-*
  // by moving down from current breakpoint
  else {
    let pointer = order.indexOf(currentBreakpoint) - 1;

    if (pointer < 0) {
      pointer = order.length - 1;
    }

    for (pointer; pointer >= 0; pointer -= 1) {
      if (!bg) {
        bg = e.target.getAttribute(`data-bg-${order[pointer]}`);
      }
    }
  }

  // Last hope - try to get data-bg
  if (!bg) {
    bg = e.target.getAttribute(`data-bg`);
  }

  if (bg) {
    e.target.style.backgroundImage = `url(${bg})`;
  }
});
