export class Nav {
  constructor() {
    this.$wrapper = $('.js-header');
    this.$burger = $('.js-burger');

    this.isOpened = false;

    this._attachListeners();
  }

  _attachListeners() {
    this.$burger.on('click', () => this.toggleState());
  }

  toggleState() {
    // eslint-disable-next-line no-unused-expressions
    this.isOpened ? this.close() : this.open();
  }

  open() {
    this.isOpened = true;
    this.$burger.addClass('is-active');
    this.$wrapper.addClass('is-opened');
  }

  close() {
    this.isOpened = false;
    this.$burger.removeClass('is-active');
    this.$wrapper.removeClass('is-opened');
  }
}
