module.exports = ({ options }) => ({
  plugins: {
    autoprefixer: {},
    'postcss-preset-env': {},
    cssnano: options.pretty
      ? false
      : {
          preset: [
            'default',
            {
              discardComments: { removeAll: true }
            }
          ]
        }
  }
});
