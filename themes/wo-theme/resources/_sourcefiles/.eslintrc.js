/*
 * ESLint project-specific rules go here.
 * */

module.exports = {
  extends: ['.eslintrc.base.js'],
  rules: {
    'no-new': 'off',
    'no-underscore-dangle': 'off'
  }
};
