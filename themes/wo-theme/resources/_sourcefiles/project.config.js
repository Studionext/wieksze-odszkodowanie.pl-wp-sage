const path = require('path');

module.exports = {
  /*
   * Root folder for all dev assets.
   * */
  context: path.resolve(__dirname, 'dev-assets'),

  /*
   * Provide app entrypoints here.
   * Prefix with:
   * js/, if it's a JS file (even when containing CSS/SCSS imports inside)
   * css/, if it's a CSS/SCSS file
   *
   * Key name is used as the asset filename
   *
   * This is used to do a proper cleanup after compiling.
   * */
  entryPoints: {
    'js/main': './app-entry.js',
    'js/critical': './app-critical.js',
  },

  /*
   * Path where built assets will go to.
   * */
  buildPath: path.resolve(__dirname, '..', '..', 'dist'),

  /*
   * Remove ALL build folder files before build.
   * The folder itself won't be removed.
   * WARNING: If the build folder contains other files, they will be lost.
   * */
  cleanBuildPathBeforeBuild: true,

  /*
   * Use webpack plugins to compress images.
   * This may break apng files (animated pngs).
   * */
  compressImages: false,

  /*
   * Base path for all the output assets.
   * */
  publicPath: '../',

  browserSync: {
    /*
     * Proxy target when using BrowserSync as serve method
     * */
    proxyTarget: 'http://localhost',
    port: 3000
  },

  /*
   * Webpack Dev Server port
   * */
  wdsPort: 8081,

  /*
   * Root path for file watcher
   * */
  watchRoot: path.resolve(__dirname, '..'),

  /*
   * Server reload will be triggered, when files listed here change.
   * Path should be relative to project directory root (where the main .php files live).
   * */
  watch: ['**/*.php', '**/*.blade.php']
};
