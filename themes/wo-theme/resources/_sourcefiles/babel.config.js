const presets = ['@babel/preset-env'];
const plugins = [
  '@babel/plugin-proposal-class-properties',
  '@babel/plugin-syntax-dynamic-import'
];

module.exports = { presets, plugins };
