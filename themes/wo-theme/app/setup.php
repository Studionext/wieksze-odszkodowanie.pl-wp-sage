<?php

namespace App;


require_once('Includes/constants.php');
require_once('Includes/sage-setup.php');
require_once('Includes/scripts.php');
require_once('Includes/styles.php');
require_once('Includes/wp-cleanup.php');
require_once('Includes/site-setup.php');
require_once('Includes/performance.php');
require_once('Includes/option-pages.php');
require_once('Includes/cpt.php');
require_once('Includes/blocks.php');



if (!isLocalhost()) {
    require_once('Includes/compression.php');
}
