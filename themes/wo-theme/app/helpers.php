<?php

namespace App;

function isLocalhost(): bool
{
    $whitelist = array('127.0.0.1', '::1', 'localhost');

    return in_array($_SERVER['REMOTE_ADDR'], $whitelist);
}


function parseSrcset(array $srcset, $acf = false): string
{
    $result = '';

    array_walk($srcset, function ($image, $descriptor) use (&$result, $acf) {
        $temp = $acf ? $image.' '.$descriptor : asset_path('img/'.$image).' '.$descriptor;
        $result .= $temp.', ';
    });

    // remove traling space and comma
    return substr($result, 0, -2);
}


function parseSizes(array $sizes, $acf = false): string
{
    $result = '';

    array_walk($sizes, function ($slotSize, $media) use (&$result, $acf) {
        if ($media) {
            $result .= "($media) ";
        }
        $result .= $slotSize.', ';
    });

    // remove traling space and comma
    return substr($result, 0, -2);
}


function inlineCss(string $path): ?string
{
    if (!file_exists($path)) {
        return null;
    }

    $css = file_get_contents($path);
    $css = preg_replace('/\/\*((?!\*\/).)*\*\//', '', $css);
    $css = preg_replace('/\s{2,}/', ' ', $css);
    $css = preg_replace('/\s*([:;{}])\s*/', '$1', $css);
    $css = preg_replace('/;}/', '}', $css);

    return "<style>$css</style>";
}
add_filter( 'wpseo_breadcrumb_separator', function( $separator ) {
    return '<span class="a-breadcrumb__separator">' . $separator . '</span>';
} );

