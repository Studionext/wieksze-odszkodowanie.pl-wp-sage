<?php

function t4d_add_cpts()
{
    register_post_type(
        'template',
        // CPT Options
        array(
            'labels' => array(
                'name' => __('Szablony'),
                'singular_name' => __('Szablon')
            ),
            'supports' => array(
                'title',
                'editor',
                'author',
                'thumbnail',
                'excerpt',
                'comments',
                'custom-fields',
                'page-attributes',
                'post-formats'
            ),
            'menu_icon' => 'dashicons-layout',
            'show_in_rest' => true,
            'hierarchical' => true,
            'show_in_nav_menus' => true,
            'public' => true,
            'has_archive' => false,
        )

    );

    register_post_type(
        'success_story',
        // CPT Options
        array(
            'labels' => array(
                'name' => __('Historie Sukcesu'),
                'singular_name' => __('Historia Sukcesu')
            ),
            'supports' => array(
                'title',
                'editor',
                'author',
                'thumbnail',
                'excerpt',
                'comments',
                'custom-fields',
                'page-attributes',
                'post-formats'
            ),
            'menu_icon' => 'dashicons-awards',
            'show_in_rest' => true,
            'hierarchical' => true,
            'show_in_nav_menus' => true,
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'historie-sukcesu'),
        )
    );
}


add_action('init', 't4d_add_cpts', 0);


add_action( 'init', 'success_story_custom_taxonomy', 0 );
 
//create a custom taxonomy name it "type" for your posts
function success_story_custom_taxonomy() {
 
  $labels = array(
    'name' => _x( 'Kategorie', 'taxonomy general name' ),
    'singular_name' => _x( 'Kategoria', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Types' ),
    'all_items' => __( 'All Types' ),
    'edit_item' => __( 'Edytuj' ), 
    'update_item' => __( 'Aktualizuj' ),
    'add_new_item' => __( 'Dodaj nową kategorię' ),
    'new_item_name' => __( 'Nowa nazwa kategorii' ),
    'menu_name' => __( 'Kategorie historii sukcesu' ),
  ); 	
 
  register_taxonomy('success_story_category',array('success_story'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
  ));
}
