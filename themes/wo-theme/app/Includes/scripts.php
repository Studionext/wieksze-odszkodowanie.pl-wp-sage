<?php

use App\Includes\Classes\AssetsProvider;


add_action('wp_enqueue_scripts', function () {
    deregisterScripts();
    enqueueThemeScripts();
});


function enqueueThemeScripts()
{
    $provider = new AssetsProvider();

    /*
     * This will enqueue all files with "main" in filename, so main.js, main.min.js, etc.
     * */
    $provider->enqueueScript('critical');
    $provider->enqueueScript('main');

    if (is_singular('machine')) {
        $provider->enqueueScript('machines');
    }
}


// Include custom jQuery
// IMPORTANT: The handle needs to stay named 'jquery' for plugins to work!
function deregisterScripts()
{
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', array(), null, false);


    // New since 5.0.0
    // Removes Gutenberg Blocks related styles

    if (!is_singular() || !is_single()) {
//        wp_dequeue_style('wp-block-library');
    }
}


