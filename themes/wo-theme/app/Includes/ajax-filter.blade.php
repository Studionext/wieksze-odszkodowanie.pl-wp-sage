<?php

add_action('wp_ajax_myfilter', 'filter_function');
add_action('wp_ajax_nopriv_myfilter', 'filter_function');
 
function filter_function(){

	if( isset( $_POST['categoryfilter'] ) )
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'success_story_category',
				'field' => 'id',
				'terms' => $_POST['categoryfilter']
			)
		);
$stories = new WP_Query( $args );

if( $stories->have_posts() ) :
    while( $stories->have_posts() ): $stories->the_post();
        ?>

          <article data-min="{{$post->acf['min'] }}" data-max="{{$post->acf['max'] }}"
                class="t-successStories__article m-successTile js-successTile">
                <a href="<?php the_permalink($post->ID) ?>">
                    @if (get_the_post_thumbnail_url($story->ID))
                    <div class="m-successTile__image lazyload" data-bg="{{ get_the_post_thumbnail_url($story->ID) }}">
                    </div>
                    @else
                    <div class="m-successTile__image empty"></div>
                    @endif
                    <div class="m-successTile__body --observe">
                        <div class="m-successTile__title">{{ $story->acf['type']['label'] }}</div>
                        <div class="m-successTile__barWrapper">
                            <div class="m-successTile__barQuotes">
                                <span>{{ $story->acf['min'] }} zł</span>
                                <span>{{ $story->acf['max'] }} zł</span>
                            </div>
                            <div class="m-successTile__bar">
                                <span data-transparent class="transparent" style="width:50%;"></span>
                                <span data-shield class="shield" style="left: 50%">
                                    <svg fill="none" viewBox="0 0 19 21">
                                        <path fill="#2B71AA"
                                            d="M19 7.773C19 13.446 15.33 18.669 9.5 21 3.826 18.731 0 13.6 0 7.773V3.58L9.5 0 19 3.58v4.193z" />
                                    </svg>
                                </span>
                                <span data-color class="color" style="width:50%;"></span>
                            </div>
                            <div class="m-successTile__barLabels">
                                <span>zaniżona kwota</span>
                                <span>wywalczona kwota</span>
                            </div>
                        </div>
                        <div class="m-successTile__description">
                            {{ auto_nbsp($story->acf['description']) }}
                        </div>
                        <div class="m-successTile__link a-button --upper">Czytaj więcej ></div>
                    </div>
                </a>
            </article>
        <?php
    endwhile;
    wp_reset_postdata();
else :
    echo 'Wybierz kategorię';
endif;

die();
}