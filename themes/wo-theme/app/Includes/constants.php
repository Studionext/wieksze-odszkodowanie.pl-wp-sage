<?php

namespace App;

define('SITE_URL', get_home_url());
define('CONSULTING_URL', get_home_url()  . '/kontakt#umow-porade-prawna');
define('TEMPLATE_DIR', get_template_directory().'/');
define('ASSETS_DIR', TEMPLATE_DIR . '../dist/');
define('TEMPLATE_URL', get_template_directory_uri().'/');
define('IMAGES', get_template_directory_uri().'/../dist/img/');
define('FONTS', get_template_directory_uri().'/../dist/fonts/');
define('VIDEO', get_template_directory_uri().'/dist/video/');
define('CSS', get_template_directory_uri().'/dist/css/');
define('JS', get_template_directory_uri().'/dist/js/');
define('VENDOR', get_template_directory_uri().'/dist/vendor/');
define('PX', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7');

