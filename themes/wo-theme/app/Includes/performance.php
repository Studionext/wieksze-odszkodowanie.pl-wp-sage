<?php

function t4d_defer_scripts( $tag, $handle ) {

    $handles_to_defer = array(
        'jquery',
        't4d_js_main',
        't4d_js_machines',
        'underscore',
        'nf-front-end-deps',
        'nf-front-end',
        'backbone',
        'backbone-marionette',
        'marionette',
        'eeb-js-frontend',
        'eeb-js-ajax-ef',

        //file-upload for NF
        'nf-fu-jquery-iframe-transport',
        'nf-fu-jquery-fileupload',
        'nf-fu-jquery-fileupload-process',
        'nf-fu-jquery-fileupload-validate',
        'nf-fu-file-upload',
        'jquery-ui-widget'

    );

    if ( ! in_array( $handle, $handles_to_defer ) ) {
        return $tag;
    }

    return str_replace( ' src', ' defer="defer" src', $tag );
}
if (!is_admin()) {
    add_filter( 'script_loader_tag', 't4d_defer_scripts', 10, 2 );
}

function t4d_async_scripts( $tag, $handle ) {

    $handles_to_async = array(
        't4d_js_critical',
    );

    if ( ! in_array( $handle, $handles_to_async ) ) {
        return $tag;
    }

    return str_replace( ' src', ' async="async" src', $tag );
}
if (!is_admin()) {
    add_filter( 'script_loader_tag', 't4d_async_scripts', 10, 2 );
}
