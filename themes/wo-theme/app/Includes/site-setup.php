<?php

namespace App;


use WP_Error;


function t4d_theme_settings()
{
    add_theme_support('post-thumbnails');
    add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));
}


add_action('after_setup_theme', __NAMESPACE__.'\\t4d_theme_settings');
// @Codex:
// If attached to a hook, it must be ‘after_setup_theme’. The ‘init’ hook may be too late for some features.


// Register default menus
function t4d_menus()
{
    register_nav_menus(
        array(
            'header-menu' => __('Header Menu'),
            'footer-menu_one' => __('Footer Menu 1'),
            'footer-menu_two' => __('Footer Menu 2')
        )
    );
}


add_action('init', __NAMESPACE__.'\\t4d_menus');


if ( ! isLocalhost()) {
    add_filter('login_errors', function () {
        return 'Something is not right.';
    });
}


// Remove /users from wp-api
// Potential security threat;
function t4d_remove_users_endpoint($endpoints)
{
    if (is_admin() || is_user_logged_in()) {
        return $endpoints;
    }

    if (isset($endpoints['/wp/v2/users'])) {
        unset($endpoints['/wp/v2/users']);
    }

    if (isset($endpoints['/wp/v2/users/(?P<id>[\d]+)'])) {
        unset($endpoints['/wp/v2/users/(?P<id>[\d]+)']);
    }

    return $endpoints;
}
add_filter('rest_endpoints', __NAMESPACE__.'\\t4d_remove_users_endpoint');


//// Allow only logged in users to use wp_rest api
//function t4d_require_log_in_for_rest_api($result)
//{
//    if (is_admin()) {
//        return $result;
//    }
//
//    if ( ! empty($result)) {
//        return $result;
//    }
//
//    if ( ! is_user_logged_in()) {
//        return new WP_Error('rest_not_logged_in', 'You are not currently logged in.', array('status' => 401));
//    }
//
//    return $result;
//}
//
//
//if ( ! is_admin()) {
////    add_filter('rest_authentication_errors', __NAMESPACE__.'\\t4d_require_log_in_for_rest_api');
//}


add_filter('excerpt_length', 'App\t4d_excerpt_length');
function t4d_excerpt_length($length)
{
    return 20;
}


// Move Yoast to bottom
function t4d_move_yoast_to_bottom()
{
    return 'low';
}


add_filter('wpseo_metabox_prio', __NAMESPACE__.'\\t4d_move_yoast_to_bottom');


