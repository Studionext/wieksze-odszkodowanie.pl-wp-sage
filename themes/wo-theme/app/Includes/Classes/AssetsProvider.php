<?php

namespace App\Includes\Classes;



use function App\config;

class AssetsProvider
{
    private $templateDir = TEMPLATE_DIR;
    private $mode;
    private $serveMethod = 'wds';
    private $assetsUrl;
    private $assetsPath;


    public function __construct()
    {
        $this->setMode();
        $this->setAssetsPath();

        $this->assetsPath = config('assets.path');
    }


    public function enqueueScript(string $name, array $dependencies = [], $inFooter = false)
    {
        // browsersync injects the assets into head.
        if ($this->serveMethod === 'browsersync') {
            return;
        }
        $filename = $this->getFilename($name, 'js');
        wp_enqueue_script("t4d_js_$name", $this->assetsUrl.'/js/'.$filename, $dependencies, '', $inFooter);
    }


    public function enqueueStyle(string $name, array $dependencies = [], $media = 'all')
    {
        // in development, styles ale handled by JS entrypoints.
        if ($this->mode === 'development') {
            return;
        }

        $filename = $this->getFilename($name, 'css');
        wp_enqueue_style("t4d_css_$name", $this->assetsUrl.'/css/'.$filename, $dependencies, '', $media);
    }


    private function setMode()
    {
        file_exists($this->templateDir.'.dev') ? $this->mode = 'development' : $this->mode = 'production';
        $this->mode === 'development' ? $this->setServeMethod() : null;
    }


    private function setAssetsPath()
    {
        if ($this->mode === 'development' && $this->serveMethod === 'wds') {
            $data = explode('|', file_get_contents($this->templateDir.'.dev'));
            $port = $data[0];
            $path = $data[1];
            $this->assetsUrl = "http://localhost:$port/$path";
        } else {
            $this->assetsUrl = config('assets.uri');
        }
    }


    private function setServeMethod()
    {
        file_get_contents($this->templateDir.'.dev') === 'browsersync' ? $this->serveMethod = 'browsersync' : null;
    }


    private function getFilename(string $name, string $extension)
    {
        if ($this->mode === 'development') {
            return "$name.$extension";
        }

        $files = glob($this->assetsPath."/$extension/$name*.$extension");
        $filepath = $files[0] ?? null;

        if ( ! $filepath) {
            return null;
        }

        return pathinfo($filepath)['basename'];
    }
}

