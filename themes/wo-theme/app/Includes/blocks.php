<?php

function wd_admin_gutenstyles() {
    wp_enqueue_script('wd-editor', get_stylesheet_directory_uri() . '/editor.js', array( 'wp-blocks', 'wp-dom' ), null, true );
}
add_action( 'enqueue_block_editor_assets', 'wd_admin_gutenstyles' );
