<?php

use App\Includes\Classes\AssetsProvider;


add_action('wp', function () {
    if (is_admin()) {
        return;
    }

    $provider = new AssetsProvider();

    /*
     * This will enqueue all files with "main" in filename, so main.css, main.min.css, etc.
     * */
    $provider->enqueueStyle('main');
}, 100);
