<?php

namespace App\Controllers;

use Sober\Controller\Controller;


class Blog extends Controller
{
    protected $template = 'blog';
    protected $acf = true;


    public function posts()
    {

        $posts = get_posts([
            'numberposts' => -1,
            'category__not_in' => array(22, 43, 6)
        ]);

        return $posts;
    }
}