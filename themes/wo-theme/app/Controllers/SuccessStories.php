<?php

namespace App\Controllers;

use Sober\Controller\Controller;


class SuccessStories extends Controller
{
    protected $template = 'success-stories';
    protected $acf = true;

    public function stories()
    {
       
                 $stories = get_posts([
                'post_type' => 'success_story',
                'numberposts' => -1,
            ]);
        $stories = array_map(function($el) {
            $fields = get_fields($el->ID);
            $el->acf = $fields;
            $el->cat_id = get_the_terms($el->ID, 'success_story_category')[0]->term_id;

            return $el;
        }, $stories);

        return $stories;
    }
}




