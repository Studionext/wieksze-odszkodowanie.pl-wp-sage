<?php

namespace App\Controllers;

use Sober\Controller\Controller;


class Contact extends Controller
{
    protected $template = 'contact';
    protected $acf = true;
}

