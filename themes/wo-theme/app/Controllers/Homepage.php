<?php

namespace App\Controllers;

use Sober\Controller\Controller;


class Homepage extends Controller
{
    protected $template = 'homepage';
    protected $acf = true;
}

