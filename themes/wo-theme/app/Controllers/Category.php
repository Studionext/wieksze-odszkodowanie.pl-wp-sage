<?php

namespace App\Controllers;

use Sober\Controller\Controller;


class Category extends Controller
{
    protected $template = 'category';
    protected $acf = true;
}