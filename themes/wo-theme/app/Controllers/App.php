<?php

namespace App\Controllers;

use Sober\Controller\Controller;


class App extends Controller
{

    public function globals()
    {
        $gl = (object) get_fields('option');

        return $gl->globals;
    }


    public static function parseTemplateData($template)
    {
        $map = [
            'list_with_icons' => 51,
            'rich_list' => 80,
            'post_rotator' => 113,
            'logos_slider' => 124,
            'steps' => 132,
            'post_media' => 144,
            'highlights' => 162,
            'why_us' => 188,
            'selected_success_stories' => 243,
            'book_a_visit' => 248,
            'blocks_slider' => 259,
            'check_list' => 404,
            'other_offers' => 422,
        ];



        $name = $template->acf_fc_layout;
        $overwrite = $template->$name->overwrite;

        if ($overwrite) {
            $fields = $template->$name;
        } else {
            $fields = get_fields($map[$name])[$name];
        }

        $fields = json_decode(json_encode($fields,), true);

        return $fields;
    }


    public static function parseNumber($number)
    {
        $number = str_replace(' ', '', $number);
        $number = str_replace('(', '', $number);
        $number = str_replace(')', '', $number);

        return $number;
    }
}