<?php

namespace App\Controllers;

use Sober\Controller\Controller;


class PressInfo extends Controller
{
    protected $template = 'press-info';
    protected $acf = true;


    public function posts()
    {
        $posts = get_posts([
            'category' => [22],
            'numberposts' => -1,
        ]);

        return $posts;
    }
}




